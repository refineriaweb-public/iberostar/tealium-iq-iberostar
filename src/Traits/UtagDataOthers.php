<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataOthers
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataOthers
{
    private static $event_cat;
    private static $event_act;
    private static $event_lbl;
    private static $event_val;
    private static $enh_action;
}
