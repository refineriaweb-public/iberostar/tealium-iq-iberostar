<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataProductImpressionList
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataProductImpressionList
{
    /** @var array Product Impression ID  */
    private static $product_impression_id;

    /** @var array Product Impression Name  */
    private static $product_impression_name;

    /** @var array Product Impression Brand  */
    private static $product_impression_brand;

    /** @var array Product Impression Variant  */
    private static $product_impression_variant;

    /** @var array Product Impression Category  */
    private static $product_impression_category;

    /** @var array Product Impression List  */
    private static $product_impression_list;

    /** @var array Product Impression Price  */
    private static $product_impression_price;

    /** @var array Product Impression Position  */
    private static $product_impression_position;

    /**
     * @return array Product Impression ID
     */
    public static function getProductImpressionId(): array
    {
        return self::$product_impression_id;
    }

    /**
     * @param array $product_impression_id Product Impression ID
     */
    public static function setProductImpressionId(array $product_impression_id): void
    {
        self::$product_impression_id = $product_impression_id;
    }

    /**
     * @return array Product Impression Name
     */
    public static function getProductImpressionName(): array
    {
        return self::$product_impression_name;
    }

    /**
     * @param array $product_impression_name Product Impression Name
     */
    public static function setProductImpressionName(array $product_impression_name): void
    {
        foreach ($product_impression_name as &$productImmpresionName) {
            $productImmpresionName = strtolower($productImmpresionName);
        }
        unset($productImmpresionName);

        self::$product_impression_name = $product_impression_name;
    }

    /**
     * @return array Product Impression Brand
     */
    public static function getProductImpressionBrand(): array
    {
        return self::$product_impression_brand;
    }

    /**
     * @param array $product_impression_brand Product Impression Brand
     */
    public static function setProductImpressionBrand(array $product_impression_brand): void
    {
        foreach ($product_impression_brand as &$productImmpresionBrand) {
            $productImmpresionBrand = strtolower($productImmpresionBrand);
        }
        unset($productImmpresionBrand);

        self::$product_impression_brand = $product_impression_brand;
    }

    /**
     * @return array Product Impression Variant
     */
    public static function getProductImpressionVariant(): array
    {
        return self::$product_impression_variant;
    }

    /**
     * @param array $product_impression_variant Product Impression Variant
     */
    public static function setProductImpressionVariant(array $product_impression_variant): void
    {
        foreach ($product_impression_variant as &$productImmpresionVariant) {
            $productImmpresionVariant = strtolower($productImmpresionVariant);
        }
        unset($productImmpresionVariant);

        self::$product_impression_variant = $product_impression_variant;
    }

    /**
     * @return array Product Impression Category
     */
    public static function getProductImpressionCategory(): array
    {
        return self::$product_impression_category;
    }

    /**
     * @param array $product_impression_category Product Impression Category
     */
    public static function setProductImpressionCategory(array $product_impression_category): void
    {
        foreach ($product_impression_category as &$productImmpresionCategory) {
            $productImmpresionCategory = strtolower($productImmpresionCategory);
        }
        unset($productImmpresionCategory);

        self::$product_impression_category = $product_impression_category;
    }

    /**
     * @return array Product Impression List
     */
    public static function getProductImpressionList(): array
    {
        return self::$product_impression_list;
    }

    /**
     * @param array $product_impression_list Product Impression List.
     * Tiene la siguiente estructura "page_section|page_type".
     */
    public static function setProductImpressionList(array $product_impression_list): void
    {
        self::$product_impression_list = $product_impression_list;
    }

    /**
     * @return array Product Impression Price
     */
    public static function getProductImpressionPrice(): array
    {
        return self::$product_impression_price;
    }

    /**
     * @param array $product_impression_price Product Impression Price
     */
    public static function setProductImpressionPrice(array $product_impression_price): void
    {
        self::$product_impression_price = $product_impression_price;
    }

    /**
     * @return array Product Impression Position
     */
    public static function getProductImpressionPosition(): array
    {
        return self::$product_impression_position;
    }

    /**
     * @param array $product_impression_position Product Impression Position
     */
    public static function setProductImpressionPosition(array $product_impression_position): void
    {
        self::$product_impression_position = $product_impression_position;
    }
}
