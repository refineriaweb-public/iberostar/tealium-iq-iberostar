<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataProduct
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataProduct
{
    /** @var string Código de régimen en inglés, ejemplos:All inclusive - Full board- Half board - Room Only- */
    private static $product_board_code;

    /** @var string País o grupo de países al que pertenece el hotel. Minusculas e inglés.  */
    private static $product_market;

    /** @var string Nombre del producto (bien tipo hotel, bien extra) */
    private static $product_name;

    /** @var string Id del hotel */
    private static $product_id;

    private static $product_category;
    private static $product_variant;
    private static $product_impression_list;
    private static $product_impression_position;
    private static $product_on_request;
    private static $product_quantity;
    private static $product_unit_price;
    private static $product_brand;

    private static $promotion_creative;
    private static $promotion_id;
    private static $promotion_name;
    private static $promotion_position;

    private static $checkout_step;

    private static $price_euro_from;
    private static $price_euro;
    private static $price_euro_tax;
    private static $price_euro_night;
    private static $price_dolar;
    private static $price_pound;
    private static $price_local;
    private static $price_local_currency;

    private static $promo_code;
    private static $promo_is_resident;
    private static $promo_is_miiberostar;

    private static $order_id;
    private static $order_store;
    private static $order_total;
    private static $order_tax;
    private static $order_promo_code;
    private static $order_currency;
    private static $order_payment_rate;
    private static $order_payment_type;

    /**
     * @return string
     */
    public static function getProductBoardCode(): string
    {
        return self::$product_board_code;
    }

    /**
     * @param string $product_board_code
     */
    public static function setProductBoardCode(string $product_board_code)
    {
        self::$product_board_code = strtoupper($product_board_code);
    }

    /**
     * @return string
     */
    public static function getProductMarket(): string
    {
        return self::$product_market;
    }

    /**
     * @param string $product_market
     */
    public static function setProductMarket(string $product_market)
    {
        self::$product_market = strtolower($product_market);
    }

    /**
     * @return string
     */
    public static function getProductName(): string
    {
        return self::$product_name;
    }

    /**
     * @param string $product_name
     */
    public static function setProductName(string $product_name)
    {
        self::$product_name = $product_name;
    }

    /**
     * @return string
     */
    public static function getProductId(): string
    {
        return self::$product_id;
    }

    /**
     * @param string $product_id
     */
    public static function setProductId(string $product_id)
    {
        self::$product_id = $product_id;
    }


}
