<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataHotel
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataHotel
{
    /** @var array Código de habitación  para cada habitación */
    private static $hotel_room_code;

    /** @var array Nombre de la habitación */
    private static $hotel_room_name;

    /** @var array Tipo de habitación */
    private static $hotel_room_category;

    /**
     * @var string Gestora inicial, se mantiene constante.
     * Si aterriza en ficha de hotel, setear con gestora del hotel,
     * si aterriza en página neutra (como home) setear con localización del usuario.
     */
    private static $hotel_headquarter_origin;

    /**
     * @var string Gestora final.
     * Inicialmente coge el valor de la inicial, pero si se cambia durante la navegación se tiene que actualizar.
     * Solo cambiará si la gestora de un hotel que está viendo pertenece a una gestora diferente a la que ya tenía.
     */
    private static $hotel_headquarter_final;

    /** @var string Código SKU del hotel  */
    private static $hotel_sku;

    /** @var string Nombre del hotel completo  */
    private static $hotel_name;

    /** @var string Nombre corto del hotel  */
    private static $hotel_short_name;

    /**
     * @var string Tipo de hotel.
     * Posibles valores: hotel, apartment, aparthotel, hotel & bungalows
     */
    private static $hotel_type;

    /** @var bool Hotel sólo para adultos o no */
    private static $hotel_is_only_adults;

    /** @var string Número de estrellas (categoría) del hotel */
    private static $hotel_stars;

    /** @var string Categoria de hotel. Posibles valores: Beaches hotels, hotels with history, city hotel */
    private static $hotel_segment;

    /**
     * @var string Si es en la playa, ciudad, etc.
     * Posibles valores: iberostar, iberostar grand, iberostar grand heritage, iberostar heritage y iberostar selection
     */
    private static $hotel_subsegment;

    /** @var string Marca del hotevent_label: iberostar, ole, tropicana, cartago, etc */
    private static $hotel_brand;

    /** @var string Array de servicios que incorpora el hotel.  */
    private static $hotel_services;

    /** @var string Recoge en una única variable con structura de json la información de los huespedes por habitacion */
    private static $hotel_host_json;

    /**
     * @return array|null Código de habitación  para cada habitación
     */
    public static function getHotelRoomCode(): ?array
    {
        return self::$hotel_room_code;
    }

    /**
     * @param array $hotel_room_code Código de habitación  para cada habitación
     */
    public static function setHotelRoomCode(array $hotel_room_code): void
    {
        self::$hotel_room_code = $hotel_room_code;
    }

    /**
     * @return array|null Nombre de la habitación
     */
    public static function getHotelRoomName(): ?array
    {
        return self::$hotel_room_name;
    }

    /**
     * @param array $hotel_room_name Nombre de la habitación
     */
    public static function setHotelRoomName(array $hotel_room_name): void
    {
        foreach ($hotel_room_name as &$roomName) {
            $roomName = strtolower($roomName);
        }
        unset($roomName);

        self::$hotel_room_name = $hotel_room_name;
    }

    /**
     * @return array|null Tipo de habitación
     */
    public static function getHotelRoomCategory(): ?array
    {
        return self::$hotel_room_category;
    }

    /**
     * @param array $hotel_room_category Tipo de habitación
     */
    public static function setHotelRoomCategory(array $hotel_room_category): void
    {
        foreach ($hotel_room_category as &$roomCategory) {
            $roomCategory = strtolower($roomCategory);
        }
        unset($roomCategory);

        self::$hotel_room_category = $hotel_room_category;
    }

    /**
     * @return string Gestora inicial, se mantiene constante.
     * Si aterriza en ficha de hotel, setear con gestora del hotel,
     * si aterriza en página neutra (como home) setear con localización del usuario.
     */
    public static function getHotelHeadquarterOrigin(): string
    {
        return self::$hotel_headquarter_origin;
    }

    /**
     * @param string $hotel_headquarter_origin Gestora inicial, se mantiene constante.
     * Si aterriza en ficha de hotel, setear con gestora del hotel,
     * si aterriza en página neutra (como home) setear con localización del usuario.
     */
    public static function setHotelHeadquarterOrigin(string $hotel_headquarter_origin): void
    {
        self::$hotel_headquarter_origin = strtolower($hotel_headquarter_origin);
    }

    /**
     * @return string Gestora final.
     * Inicialmente coge el valor de la inicial, pero si se cambia durante la navegación se tiene que actualizar.
     * Solo cambiará si la gestora de un hotel que está viendo pertenece a una gestora diferente a la que ya tenía.
     */
    public static function getHotelHeadquarterFinal(): string
    {
        return self::$hotel_headquarter_final;
    }

    /**
     * @param string $hotel_headquarter_final Gestora final.
     * Inicialmente coge el valor de la inicial, pero si se cambia durante la navegación se tiene que actualizar.
     * Solo cambiará si la gestora de un hotel que está viendo pertenece a una gestora diferente a la que ya tenía.
     */
    public static function setHotelHeadquarterFinal(string $hotel_headquarter_final): void
    {
        self::$hotel_headquarter_final = strtolower($hotel_headquarter_final);
    }

    /**
     * @return string Código SKU del hotel
     */
    public static function getHotelSku(): string
    {
        return self::$hotel_sku;
    }

    /**
     * @param string $hotel_sku Código SKU del hotel
     */
    public static function setHotelSku(string $hotel_sku): void
    {
        self::$hotel_sku = $hotel_sku;
    }

    /**
     * @return string Nombre del hotel completo
     */
    public static function getHotelName(): string
    {
        return self::$hotel_name;
    }

    /**
     * @param string $hotel_name Nombre del hotel completo
     */
    public static function setHotelName(string $hotel_name): void
    {
        self::$hotel_name = strtolower($hotel_name);
    }

    /**
     * @return string Nombre corto del hotel
     */
    public static function getHotelShortName(): string
    {
        return self::$hotel_short_name;
    }

    /**
     * @param string $hotel_short_name Nombre corto del hotel
     */
    public static function setHotelShortName(string $hotel_short_name): void
    {
        self::$hotel_short_name = strtoupper($hotel_short_name);
    }

    /**
     * @return string Tipo de hotel.
     * Posibles valores: hotel, apartment, aparthotel, hotel & bungalows
     */
    public static function getHotelType(): string
    {
        return self::$hotel_type;
    }

    /**
     * @param string $hotel_type Tipo de hotel.
     * Posibles valores: hotel, apartment, aparthotel, hotel & bungalows
     */
    public static function setHotelType(string $hotel_type): void
    {
        self::$hotel_type = strtoupper($hotel_type);
    }

    /**
     * @return bool Hotel sólo para adultos o no
     */
    public static function isHotelIsOnlyAdults(): bool
    {
        return self::$hotel_is_only_adults;
    }

    /**
     * @param bool $hotel_is_only_adults Hotel sólo para adultos o no
     */
    public static function setHotelIsOnlyAdults(bool $hotel_is_only_adults): void
    {
        self::$hotel_is_only_adults = $hotel_is_only_adults;
    }

    /**
     * @return string Número de estrellas (categoría) del hotel
     */
    public static function getHotelStars(): string
    {
        return self::$hotel_stars;
    }

    /**
     * @param string $hotel_stars Número de estrellas (categoría) del hotel
     */
    public static function setHotelStars(string $hotel_stars): void
    {
        self::$hotel_stars = $hotel_stars;
    }

    /**
     * @return string Categoria de hotel. Posibles valores: Beaches hotels, hotels with history, city hotel
     */
    public static function getHotelSegment(): string
    {
        return self::$hotel_segment;
    }

    /**
     * @param string $hotel_segment Categoria de hotel. Posibles valores: Beaches hotels, hotels with history, city hotel
     */
    public static function setHotelSegment(string $hotel_segment): void
    {
        self::$hotel_segment = $hotel_segment;
    }

    /**
     * @return string Subcategoria de hotel. Si es en la playa, ciudad, etc.
     * Posibles valores: iberostar, iberostar grand, iberostar grand heritage, iberostar heritage y iberostar selection
     */
    public static function getHotelSubsegment(): string
    {
        return self::$hotel_subsegment;
    }

    /**
     * @param string $hotel_subsegment Si es en la playa, ciudad, etc.
     * Posibles valores: iberostar, iberostar grand, iberostar grand heritage, iberostar heritage y iberostar selection
     */
    public static function setHotelSubsegment(string $hotel_subsegment): void
    {
        self::$hotel_subsegment = $hotel_subsegment;
    }

    /**
     * @return string Marca del hotevent_label: iberostar, ole, tropicana, cartago, etc
     */
    public static function getHotelBrand(): string
    {
        return self::$hotel_brand;
    }

    /**
     * @param string $hotel_brand Marca del hotevent_label: iberostar, ole, tropicana, cartago, etc
     */
    public static function setHotelBrand(string $hotel_brand): void
    {
        self::$hotel_brand = strtolower($hotel_brand);
    }

    /**
     * @return string Array de servicios que incorpora el hotel.
     */
    public static function getHotelServices(): string
    {
        return self::$hotel_services;
    }

    /**
     * @param string $hotel_services Array de servicios que incorpora el hotel.
     */
    public static function setHotelServices(string $hotel_services): void
    {
        self::$hotel_services = $hotel_services;
    }

    /**
     * @return string Recoge en una única variable con structura de json la información de los huespedes por habitacion
     */
    public static function getHotelHostJson(): string
    {
        return self::$hotel_host_json;
    }

    /**
     * @param string $hotel_host_json Recoge en una única variable con structura de json la información de los huespedes por habitacion
     */
    public static function setHotelHostJson(string $hotel_host_json): void
    {
        self::$hotel_host_json = $hotel_host_json;
    }
}
