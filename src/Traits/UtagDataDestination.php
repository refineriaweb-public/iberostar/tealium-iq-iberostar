<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataDestination
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataDestination
{
    /** @var string Continente al cual pertenece el destino que se esta visualizando o se ha reservado */
    private static $destination_continent;

    /**
     * @var string País al cual pertenece el destino que se esta visualizando o se ha reservado.
     * En el caso de bodas será el país del menú en el cual estás navegando
     */
    private static $destination_country;

    /** @var string Código ISO (2 letras) del país que se esta visualizando o se ha reservado */
    private static $destination_country_code;

    /**
     * @var string Ciudad a la que pertenece.
     * Esta variable seguirá la estructura jerárquica de la web es decir.
     * Para barcelona, europe será el destination_continent, spain el destination_country, barcelona el destination_city, no tendrá destination_area y destination_zone.
     * En el caso de playa de muro, sera:
     *      destination_continent europe,
     *      destination_country spain,
     *      destination_city balearic islands,
     *      destination_area majorca
     *      destination_zone playa de muro.
     */
    private static $destination_city;

    /** @var string Área a la cual pertenece el destino que se esta visualizando o se ha reservado */
    private static $destination_area;

    /** @var string Zona a la cual pertenece el destino que se esta visualizando o se ha reservado */
    private static $destination_zone;

    /**
     * @return string
     */
    public static function getDestinationContinent(): string
    {
        return self::$destination_continent;
    }

    /**
     * @param string $destination_continent
     */
    public static function setDestinationContinent(string $destination_continent): void
    {
        self::$destination_continent = strtolower($destination_continent);
    }

    /**
     * @return string
     */
    public static function getDestinationCountry(): string
    {
        return self::$destination_country;
    }

    /**
     * @param string $destination_country
     */
    public static function setDestinationCountry(string $destination_country): void
    {
        self::$destination_country = strtolower($destination_country);
    }

    /**
     * @return string
     */
    public static function getDestinationCountryCode(): string
    {
        return self::$destination_country_code;
    }

    /**
     * @param string $destination_country_code
     */
    public static function setDestinationCountryCode(string $destination_country_code): void
    {
        self::$destination_country_code = substr(strtoupper($destination_country_code), 0, 2);
    }

    /**
     * @return string
     */
    public static function getDestinationCity(): string
    {
        return self::$destination_city;
    }

    /**
     * @param string $destination_city
     */
    public static function setDestinationCity(string $destination_city): void
    {
        self::$destination_city = strtolower($destination_city);
    }

    /**
     * @return string
     */
    public static function getDestinationArea(): string
    {
        return self::$destination_area;
    }

    /**
     * @param string $destination_area
     */
    public static function setDestinationArea(string $destination_area): void
    {
        self::$destination_area = strtolower($destination_area);
    }

    /**
     * @return string
     */
    public static function getDestinationZone(): string
    {
        return self::$destination_zone;
    }

    /**
     * @param string $destination_zone
     */
    public static function setDestinationZone(string $destination_zone): void
    {
        self::$destination_zone = strtolower($destination_zone);
    }
}
