<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataUser
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataUser
{
    private static $user_id;
    private static $user_level;
    private static $user_points;
    private static $user_gender;
    private static $user_age;
    private static $user_date_born;
    private static $user_country;
    private static $user_country_code;
    private static $user_city;
    private static $user_zip_code;
    private static $user_email_sha256;
    private static $user_email_md5;
    private static $user_preference_room;
    private static $user_preference_room_level;
    private static $user_preference_vacations;
    private static $user_preference_smoking;
    private static $user_favorite_hotels;
}
