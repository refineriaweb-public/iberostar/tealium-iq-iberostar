<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataCampaign
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataCampaign
{
    /** @var string Fuente de la campaña. Ejemplo: google */
    private static $campaign_source = "not set";

    /** @var string Medio por el cual se ha llevado a cabo la campaña. Ejemplo: cpc */
    private static $campaign_medium = "not set";

    /** @var string Nombre de la campaña. Ejemplo: summer_2018 */
    private static $campaign_name = "not set";

    /** @var string Terminos de la campaña. Ejemplo: summer vacation resort */
    private static $campaign_term = "not set";

    /** @var string Contenidos de la campaña. Ejemplo: original y cambio de imagen link: ejemplo.com */
    private static $campaign_content = "not set";

    /** @var string Partner de la campaña. Ejemplo: match real time bidding */
    private static $campaign_cp = "not set";

    /**
     * @return string Fuente de la campaña. Ejemplo: google
     */
    public static function getCampaignSource(): string
    {
        return self::$campaign_source;
    }

    /**
     * @param string $campaign_source Fuente de la campaña. Ejemplo: google
     */
    public static function setCampaignSource(string $campaign_source): void
    {
        self::$campaign_source = strtolower($campaign_source);
    }

    /**
     * @return string Medio por el cual se ha llevado a cabo la campaña. Ejemplo: cpc
     */
    public static function getCampaignMedium(): string
    {
        return self::$campaign_medium;
    }

    /**
     * @param string $campaign_medium Medio por el cual se ha llevado a cabo la campaña. Ejemplo: cpc
     */
    public static function setCampaignMedium(string $campaign_medium): void
    {
        self::$campaign_medium = strtolower($campaign_medium);
    }

    /**
     * @return string Nombre de la campaña. Ejemplo: summer_2018
     */
    public static function getCampaignName(): string
    {
        return self::$campaign_name;
    }

    /**
     * @param string $campaign_name Nombre de la campaña. Ejemplo: summer_2018
     */
    public static function setCampaignName(string $campaign_name): void
    {
        self::$campaign_name = strtolower(snake_case($campaign_name, '_'));
    }

    /**
     * @return string Terminos de la campaña. Ejemplo: summer vacation resort
     */
    public static function getCampaignTerm(): string
    {
        return self::$campaign_term;
    }

    /**
     * @param string $campaign_term Terminos de la campaña. Ejemplo: summer vacation resort
     */
    public static function setCampaignTerm(string $campaign_term): void
    {
        self::$campaign_term = strtolower($campaign_term);
    }

    /**
     * @return string Contenidos de la campaña. Ejemplo: original y cambio de imagen link: ejemplo.com
     */
    public static function getCampaignContent(): string
    {
        return self::$campaign_content;
    }

    /**
     * @param string $campaign_content Contenidos de la campaña. Ejemplo: original y cambio de imagen link: ejemplo.com
     */
    public static function setCampaignContent(string $campaign_content): void
    {
        self::$campaign_content = strtolower($campaign_content);
    }

    /**
     * @return string Partner de la campaña. Ejemplo: match real time bidding
     */
    public static function getCampaignCp(): string
    {
        return self::$campaign_cp;
    }

    /**
     * @param string $campaign_cp Partner de la campaña. Ejemplo: match real time bidding
     */
    public static function setCampaignCp(string $campaign_cp): void
    {
        self::$campaign_cp = strtolower($campaign_cp);
    }
}
