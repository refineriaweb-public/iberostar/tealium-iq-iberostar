<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataOffers
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataOffers
{
    private static $offers_title;
    private static $offers_description;
    private static $offers_hotel;
    private static $offers_detination;
    private static $offers_discount;
    private static $offers_values;
    private static $offers_cod_prom;
    private static $offers_url_img;
}
