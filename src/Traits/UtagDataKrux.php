<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataKrux
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataKrux
{
    /** @var string Identificador de Krux. Se recoge del localstorage.  */
    private static $krux_id;

    /** @var string Segmentos de Krux. Se recoge del localstorage.  */
    private static $krux_segments;

    /**
     * @return string
     */
    public static function getKruxId(): string
    {
        return self::$krux_id;
    }

    /**
     * @param string $krux_id
     */
    public static function setKruxId(string $krux_id): void
    {
        self::$krux_id = $krux_id;
    }

    /**
     * @return string
     */
    public static function getKruxSegments(): string
    {
        return self::$krux_segments;
    }

    /**
     * @param string $krux_segments
     */
    public static function setKruxSegments(string $krux_segments): void
    {
        self::$krux_segments = $krux_segments;
    }
}
