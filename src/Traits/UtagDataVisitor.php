<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataVisitor
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataVisitor
{
    /** @var string Cookie de GA [u] */
    private static $visitor_id;

    /** @var string Cookie de session que se le asigna IT. [s] */
    private static $visitor_session_id;

    /**
     * @var string Tipo de visitante.
     * Una vez que pasa al nivel superior, se debe mantener. En caso de igual nivel, coger el último.
     * Posibles valores: standard o miiberostar o weeding o agencie.
     * El nivel jerárquico en caso de que un usuario pertenezca a varios grupos será el siguiente:
     *    standard < miiberostar =  weeding < agencie.
     *    miiberostar: usuario con cuenta en miIberostar
     *    weeding: para la web de bodas. Usuario con cuenta en myweeding
     *    agencie: agencias profesionales (para las webs que apliquen)
     *    standard: el usuario por defecto que no cumple ninguna de las anteriores.
     */
    private static $visitor_type = "standard";

    /** @var string País de la ip del visitante. Minusculas e inglés */
    private static $visitor_country = "";

    /** @var string Código iso del páis de la ip del visitante */
    private static $visitor_country_code;

    /** @var string Indica si el visitante es nuevo o repetidor */
    private static $visitor_is_repeater;

    /** @var string Recurrencia con la que el usuario ha visitado el site. Qué número de días han pasado desde la última visita */
    private static $visitor_recurrence;

    /** @var bool Indica si el visitante es comprador o no comprador. Si el usuario ya ha comprado/reservado previamente. */
    private static $visitor_is_buyer = false;

    /**
     * @var int Indica si el visitante tiene una reserva activa.
     * 0 = no tiene reserva
     * 1 = Tiene reserva y no la ha consumido
     * 2 = Esta actualmente en el hotel (si coinciden las fechas)
     */
    private static $visitor_has_reservation = 0;

    /** @var bool Indica si el visitante está logado o no */
    private static $visitor_is_logged = false;

    /** @var string Indica gestora a la que pertenece el usuario. S extrae a partir de la ip  */
    private static $visitor_headquarter_market;

    /** @var int Cuenta el número de sesiones del visitante. Número de sesiones del usuario hasta ese momento. */
    private static $visitor_session_count;

    /** @var string Identificador interno de mercado. Array con los diferentes mercados. Debe ir en Mayúsculas */
    private static $visitor_internal_market;

    /**
     * @var string Agrupación de los diferentes mercados donde se encuentran los hoteles.
     * Posibles valores: mature, strategy, tactic. (ignorar, se hará a través de Tealium con una LUT)
     */
    private static $visitor_strategy_market;

    /**
     * @return string Cookie de GA [u]
     */
    public static function getVisitorId(): string
    {
        return self::$visitor_id;
    }

    /**
     * @param string $visitor_id Cookie de GA [u]
     */
    public static function setVisitorId(string $visitor_id): void
    {
        self::$visitor_id = $visitor_id;
    }

    /**
     * @return string Cookie de session que se le asigna IT. [s]
     */
    public static function getVisitorSessionId(): string
    {
        return self::$visitor_session_id;
    }

    /**
     * @param string $visitor_session_id Cookie de session que se le asigna IT. [s]
     */
    public static function setVisitorSessionId(string $visitor_session_id): void
    {
        self::$visitor_session_id = $visitor_session_id;
    }

    /**
     * @return string Tipo de visitante.
     * Una vez que pasa al nivel superior, se debe mantener. En caso de igual nivel, coger el último.
     * Posibles valores: standard o miiberostar o weeding o agencie.
     * El nivel jerárquico en caso de que un usuario pertenezca a varios grupos será el siguiente:
     *    standard < miiberostar =  weeding < agencie.
     *    miiberostar: usuario con cuenta en miIberostar
     *    weeding: para la web de bodas. Usuario con cuenta en myweeding
     *    agencie: agencias profesionales (para las webs que apliquen)
     *    standard: el usuario por defecto que no cumple ninguna de las anteriores.
     */
    public static function getVisitorType(): string
    {
        return self::$visitor_type;
    }

    /**
     * @param string $visitor_type Tipo de visitante.
     * Una vez que pasa al nivel superior, se debe mantener. En caso de igual nivel, coger el último.
     * Posibles valores: standard o miiberostar o weeding o agencie.
     * El nivel jerárquico en caso de que un usuario pertenezca a varios grupos será el siguiente:
     *    standard < miiberostar =  weeding < agencie.
     *    miiberostar: usuario con cuenta en miIberostar
     *    weeding: para la web de bodas. Usuario con cuenta en myweeding
     *    agencie: agencias profesionales (para las webs que apliquen)
     *    standard: el usuario por defecto que no cumple ninguna de las anteriores.
     */
    public static function setVisitorType(string $visitor_type): void
    {
        self::$visitor_type = $visitor_type;
    }

    /**
     * @return string País de la ip del visitante. Minusculas e inglés
     */
    public static function getVisitorCountry(): string
    {
        return self::$visitor_country;
    }

    /**
     * @param string $visitor_country País de la ip del visitante. Minusculas e inglés
     */
    public static function setVisitorCountry(string $visitor_country): void
    {
        self::$visitor_country = strtolower($visitor_country);
    }

    /**
     * @return string Código iso del páis de la ip del visitante
     */
    public static function getVisitorCountryCode(): string
    {
        return self::$visitor_country_code;
    }

    /**
     * @param string $visitor_country_code Código iso del páis de la ip del visitante
     */
    public static function setVisitorCountryCode(string $visitor_country_code): void
    {
        self::$visitor_country_code = strtoupper($visitor_country_code);
    }

    /**
     * @return string Indica si el visitante es nuevo o repetidor
     */
    public static function getVisitorIsRepeater(): string
    {
        return self::$visitor_is_repeater;
    }

    /**
     * @param string $visitor_is_repeater Indica si el visitante es nuevo o repetidor
     */
    public static function setVisitorIsRepeater(string $visitor_is_repeater): void
    {
        self::$visitor_is_repeater = $visitor_is_repeater;
    }

    /**
     * @return string Recurrencia con la que el usuario ha visitado el site. Qué número de días han pasado desde la última visita
     */
    public static function getVisitorRecurrence(): string
    {
        return self::$visitor_recurrence;
    }

    /**
     * @param string $visitor_recurrence Recurrencia con la que el usuario ha visitado el site. Qué número de días han pasado desde la última visita
     */
    public static function setVisitorRecurrence(string $visitor_recurrence): void
    {
        self::$visitor_recurrence = $visitor_recurrence;
    }

    /**
     * @return bool Indica si el visitante es comprador o no comprador. Si el usuario ya ha comprado/reservado previamente.
     */
    public static function getVisitorIsBuyer(): bool
    {
        return self::$visitor_is_buyer;
    }

    /**
     * @param bool $visitor_is_buyer Indica si el visitante es comprador o no comprador. Si el usuario ya ha comprado/reservado previamente.
     */
    public static function setVisitorIsBuyer(bool $visitor_is_buyer): void
    {
        self::$visitor_is_buyer = $visitor_is_buyer;
    }

    /**
     * @return int Indica si el visitante tiene una reserva activa.
     * 0 = no tiene reserva
     * 1 = Tiene reserva y no la ha consumido
     * 2 = Esta actualmente en el hotel (si coinciden las fechas)
     */
    public static function getVisitorHasReservation(): int
    {
        return self::$visitor_has_reservation;
    }

    /**
     * @param int $visitor_has_reservation Indica si el visitante tiene una reserva activa.
     * 0 = no tiene reserva
     * 1 = Tiene reserva y no la ha consumido
     * 2 = Esta actualmente en el hotel (si coinciden las fechas)
     */
    public static function setVisitorHasReservation(int $visitor_has_reservation): void
    {
        self::$visitor_has_reservation = $visitor_has_reservation;
    }

    /**
     * @return bool Indica si el visitante está logado o no
     */
    public static function getVisitorIsLogged(): bool
    {
        return self::$visitor_is_logged;
    }

    /**
     * @param bool $visitor_is_logged Indica si el visitante está logado o no
     */
    public static function setVisitorIsLogged(bool $visitor_is_logged): void
    {
        self::$visitor_is_logged = $visitor_is_logged;
    }

    /**
     * @return string Indica gestora a la que pertenece el usuario. S extrae a partir de la ip
     */
    public static function getVisitorHeadquarterMarket(): string
    {
        return self::$visitor_headquarter_market;
    }

    /**
     * @param string $visitor_headquarter_market Indica gestora a la que pertenece el usuario. S extrae a partir de la ip
     */
    public static function setVisitorHeadquarterMarket(string $visitor_headquarter_market): void
    {
        self::$visitor_headquarter_market = strtolower($visitor_headquarter_market);
    }

    /**
     * @return int Cuenta el número de sesiones del visitante. Número de sesiones del usuario hasta ese momento.
     */
    public static function getVisitorSessionCount(): int
    {
        return self::$visitor_session_count;
    }

    /**
     * @param int $visitor_session_count Cuenta el número de sesiones del visitante. Número de sesiones del usuario hasta ese momento
     */
    public static function setVisitorSessionCount(int $visitor_session_count): void
    {
        self::$visitor_session_count = $visitor_session_count;
    }

    /**
     * @return string Identificador interno de mercado. Array con los diferentes mercados. Debe ir en Mayúsculas
     */
    public static function getVisitorInternalMarket(): string
    {
        return self::$visitor_internal_market;
    }

    /**
     * @param string $visitor_internal_market Identificador interno de mercado. Array con los diferentes mercados. Debe ir en Mayúsculas
     */
    public static function setVisitorInternalMarket(string $visitor_internal_market): void
    {
        self::$visitor_internal_market = strtoupper($visitor_internal_market);
    }

    /**
     * @return string Identificador interno de mercado. Array con los diferentes mercados. Debe ir en Mayúsculas
     */
    public static function getVisitorStrategyMarket(): string
    {
        return self::$visitor_strategy_market;
    }

    /**
     * @param string $visitor_strategy_market Identificador interno de mercado. Array con los diferentes mercados. Debe ir en Mayúsculas
     */
    public static function setVisitorStrategyMarket(string $visitor_strategy_market): void
    {
        self::$visitor_strategy_market = $visitor_strategy_market;
    }
}
