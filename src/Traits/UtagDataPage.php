<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataPage
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataPage
{
    /** @var string Url completo de la página */
    private static $page_url;

    /** @var string Dominio de la página */
    private static $page_domain;

    /** @var string Url sin el dominio y los parámetros */
    private static $page_pathname;

    /** @var string Parámetros de la url */
    private static $page_query_string;

    /** @var string Categoría de la página (Primer nivel). Ver pestaña 02. Content Grouping. */
    private static $page_section;

    /** @var string Categoría de la página (Segundo nivel). Vier pestaña 02. Content Grouping. */
    private static $page_type;

    /** @var string Categoría de la página (Tercer nivel) */
    private static $page_category;

    /** @var string Número de variación del test de Optimize.  */
    private static $page_variation;

    /**
     * @var string Título de la página. Los page_title tiene que recogerse igual que el meta title.
     * No lo modificamos ni normalizamos.
     */
    private static $page_title;

    /** @var string Descripción de la página */
    private static $page_description;

    /** @var int Número de palabras en la página */
    private static $page_words_count;

    /** @var string Contenido de la etiqueta canonical */
    private static $page_canonical;

    /** @var string Contenido de la etiqueta meta robots */
    private static $page_robots;

    /** @var string Código del idioma de la página. Ej. España: es */
    private static $page_language;

    /** @var string Código de moneda de la página. Ej. Euro: EUR */
    private static $page_currency;

    /** @var string Diferencia entre CMS y CRS */
    private static $page_platform;

    /**
     * @var bool Indica si el usuario está logado o no logado (true, false).
     * Es necesario consultar este valor en cada página. No coger el valor de sesión.
     */
    private static $page_is_logged;

    /** @var bool Indica si la página tiene adsblocker o algun tipo de bloqueador de anuncios */
    private static $page_has_adsblocker;

    /**
     * @var int Contador de imagenes clickadas.
     * Cuenta tanto click sobre la imagen, como tap y movimiento sobre la imagen, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones, junto con el evento #35,
     * cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para imágenes de galerías o módulos con clara interacción del usuario con las imágenes
     */
    private static $page_images_clicked;

    /**
     * @var int Contador de videos clickados.
     * Cuenta tanto click sobre el video, como tap y movimiento sobre el video, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones,
     * junto con el evento #35, cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para videos de galerías o módulos con clara interacción del usuario con los videos
     */
    private static $page_videos_clicked;

    /**
     * @var int Contador de videos 360 clickados.
     * Cuenta tanto click sobre el video, como tap y movimiento sobre el video, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones,
     * junto con el evento #35, cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para videos de galerías o módulos con clara interacción del usuario con los videos
     */
    private static $page_360_views_clicked;

    /**
     * @var string Indica la web de navegación. Mantiene el valor en el CRS.
     * Ejemplo: ole Hotels. Utilizar también para identificar el call center.
     */
    private static $page_site_origin;

    /** @var string Indica el estilo CSS al que pertenece la página */
    private static $page_style;

    /**
     * @return string Url completo de la página
     */
    public static function getPageUrl(): string
    {
        return self::$page_url;
    }

    /**
     * @param string $page_url Url completo de la página
     */
    public static function setPageUrl(string $page_url): void
    {
        self::$page_url = $page_url;
    }

    /**
     * @return string Dominio de la página
     */
    public static function getPageDomain(): string
    {
        return self::$page_domain;
    }

    /**
     * @param string $page_domain Dominio de la página
     */
    public static function setPageDomain(string $page_domain): void
    {
        self::$page_domain = $page_domain;
    }

    /**
     * @return string Url sin el dominio y los parámetros
     */
    public static function getPagePathname(): string
    {
        return self::$page_pathname;
    }

    /**
     * @param string $page_pathname Url sin el dominio y los parámetros
     */
    public static function setPagePathname(string $page_pathname): void
    {
        if (substr($page_pathname, 0, 1) !== '/') {
            $page_pathname = '/' . $page_pathname;
        }

        self::$page_pathname = $page_pathname;
    }

    /**
     * @return string Parámetros de la url
     */
    public static function getPageQueryString(): string
    {
        return self::$page_query_string;
    }

    /**
     * @param string $page_query_string Parámetros de la url
     */
    public static function setPageQueryString(string $page_query_string): void
    {
        self::$page_query_string = $page_query_string;
    }

    /**
     * @return string Categoría de la página (Primer nivel). Ver pestaña 02. Content Grouping.
     */
    public static function getPageSection(): string
    {
        return self::$page_section;
    }

    /**
     * @param string $page_section Categoría de la página (Primer nivel). Ver pestaña 02. Content Grouping.
     */
    public static function setPageSection(string $page_section): void
    {
        self::$page_section = $page_section;
    }

    /**
     * @return string Categoría de la página (Segundo nivel). Vier pestaña 02. Content Grouping.
     */
    public static function getPageType(): string
    {
        return self::$page_type;
    }

    /**
     * @param string $page_type Categoría de la página (Segundo nivel). Vier pestaña 02. Content Grouping.
     */
    public static function setPageType(string $page_type): void
    {
        self::$page_type = $page_type;
    }

    /**
     * @return string Categoría de la página (Tercer nivel)
     */
    public static function getPageCategory(): string
    {
        return self::$page_category;
    }

    /**
     * @param string $page_category Categoría de la página (Tercer nivel)
     */
    public static function setPageCategory(string $page_category): void
    {
        self::$page_category = $page_category;
    }

    /**
     * @return string Número de variación del test de Optimize.
     */
    public static function getPageVariation(): string
    {
        return self::$page_variation;
    }

    /**
     * @param string $page_variation Número de variación del test de Optimize.
     */
    public static function setPageVariation(string $page_variation): void
    {
        self::$page_variation = $page_variation;
    }

    /**
     * @return string Título de la página. Los page_title tiene que recogerse igual que el meta title.
     * No lo modificamos ni normalizamos.
     */
    public static function getPageTitle(): string
    {
        return self::$page_title;
    }

    /**
     * @param string $page_title Título de la página. Los page_title tiene que recogerse igual que el meta title.
     * No lo modificamos ni normalizamos.
     */
    public static function setPageTitle(string $page_title): void
    {
        self::$page_title = $page_title;
    }

    /**
     * @return string Descripción de la página
     */
    public static function getPageDescription(): string
    {
        return self::$page_description;
    }

    /**
     * @param string $page_description Descripción de la página
     */
    public static function setPageDescription(string $page_description): void
    {
        self::$page_description = $page_description;
    }

    /**
     * @return int Número de palabras en la página
     */
    public static function getPageWordsCount(): int
    {
        return self::$page_words_count;
    }

    /**
     * @param int $page_words_count Número de palabras en la página
     */
    public static function setPageWordsCount(int $page_words_count): void
    {
        self::$page_words_count = $page_words_count;
    }

    /**
     * @return string Contenido de la etiqueta canonical
     */
    public static function getPageCanonical(): string
    {
        return self::$page_canonical;
    }

    /**
     * @param string $page_canonical Contenido de la etiqueta canonical
     */
    public static function setPageCanonical(string $page_canonical): void
    {
        self::$page_canonical = $page_canonical;
    }

    /**
     * @return string Contenido de la etiqueta meta robots
     */
    public static function getPageRobots(): string
    {
        return self::$page_robots;
    }

    /**
     * @param string $page_robots Contenido de la etiqueta meta robots
     */
    public static function setPageRobots(string $page_robots): void
    {
        self::$page_robots = $page_robots;
    }

    /**
     * @return string Código del idioma de la página. Ej. España: es
     */
    public static function getPageLanguage(): string
    {
        return self::$page_language;
    }

    /**
     * @param string $page_language Código del idioma de la página. Ej. España: es
     */
    public static function setPageLanguage(string $page_language): void
    {
        self::$page_language = $page_language;
    }

    /**
     * @return string Código de moneda de la página. Ej. Euro: EUR
     */
    public static function getPageCurrency(): string
    {
        return self::$page_currency;
    }

    /**
     * @param string $page_currency Código de moneda de la página. Ej. Euro: EUR
     */
    public static function setPageCurrency(string $page_currency): void
    {
        self::$page_currency = $page_currency;
    }

    /**
     * @return string Diferencia entre CMS y CRS
     */
    public static function getPagePlatform(): string
    {
        return self::$page_platform;
    }

    /**
     * @param string $page_platform Diferencia entre CMS y CRS
     */
    public static function setPagePlatform(string $page_platform): void
    {
        self::$page_platform = $page_platform;
    }

    /**
     * @return bool Indica si el usuario está logado o no logado (true, false).
     * Es necesario consultar este valor en cada página. No coger el valor de sesión.
     */
    public static function isPageIsLogged(): bool
    {
        return self::$page_is_logged;
    }

    /**
     * @param bool $page_is_logged Indica si el usuario está logado o no logado (true, false).
     * Es necesario consultar este valor en cada página. No coger el valor de sesión.
     */
    public static function setPageIsLogged(bool $page_is_logged): void
    {
        self::$page_is_logged = $page_is_logged;
    }

    /**
     * @return bool Indica si la página tiene adsblocker o algun tipo de bloqueador de anuncios
     */
    public static function isPageHasAdsblocker(): bool
    {
        return self::$page_has_adsblocker;
    }

    /**
     * @param bool $page_has_adsblocker Indica si la página tiene adsblocker o algun tipo de bloqueador de anuncios
     */
    public static function setPageHasAdsblocker(bool $page_has_adsblocker): void
    {
        self::$page_has_adsblocker = $page_has_adsblocker;
    }

    /**
     * @return int Contador de imagenes clickadas.
     * Cuenta tanto click sobre la imagen, como tap y movimiento sobre la imagen, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones,
     * junto con el evento #35, cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para imágenes de galerías o módulos con clara interacción del usuario con las imágenes
     */
    public static function getPageImagesClicked(): int
    {
        return self::$page_images_clicked;
    }

    /**
     * @param int $page_images_clicked Contador de imagenes clickadas.
     * Cuenta tanto click sobre la imagen, como tap y movimiento sobre la imagen, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones, junto con el evento #35,
     * cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para imágenes de galerías o módulos con clara interacción del usuario con las imágenes
     */
    public static function setPageImagesClicked(int $page_images_clicked): void
    {
        self::$page_images_clicked = $page_images_clicked;
    }

    /**
     * @return int Contador de videos clickados.
     * Cuenta tanto click sobre el video, como tap y movimiento sobre el video, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones,
     * junto con el evento #35, cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para videos de galerías o módulos con clara interacción del usuario con los videos
     */
    public static function getPageVideosClicked(): int
    {
        return self::$page_videos_clicked;
    }

    /**
     * @param int $page_videos_clicked Contador de videos clickados.
     * Cuenta tanto click sobre el video, como tap y movimiento sobre el video, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones,
     * junto con el evento #35, cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para videos de galerías o módulos con clara interacción del usuario con los videos
     */
    public static function setPageVideosClicked(int $page_videos_clicked): void
    {
        self::$page_videos_clicked = $page_videos_clicked;
    }

    /**
     * @return int Contador de videos 360 clickados.
     * Cuenta tanto click sobre el video, como tap y movimiento sobre el video, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones,
     * junto con el evento #35, cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para videos de galerías o módulos con clara interacción del usuario con los videos
     */
    public static function getPage360ViewsClicked(): int
    {
        return self::$page_360_views_clicked;
    }

    /**
     * @param int $page_360_views_clicked Contador de videos 360 clickados.
     * Cuenta tanto click sobre el video, como tap y movimiento sobre el video, como click en las flechitas.
     * Enviar esta variable con la acumulación de interacciones,
     * junto con el evento #35, cuando el usuario salga de la página (evento JS beforeunload).
     * Sólo aplica para videos de galerías o módulos con clara interacción del usuario con los videos
     */
    public static function setPage360ViewsClicked(int $page_360_views_clicked): void
    {
        self::$page_360_views_clicked = $page_360_views_clicked;
    }

    /**
     * @return string Indica la web de navegación. Mantiene el valor en el CRS.
     * Ejemplo: ole Hotels. Utilizar también para identificar el call center.
     */
    public static function getPageSiteOrigin(): string
    {
        return self::$page_site_origin;
    }

    /**
     * @param string $page_site_origin Indica la web de navegación. Mantiene el valor en el CRS.
     * Ejemplo: ole Hotels. Utilizar también para identificar el call center.
     */
    public static function setPageSiteOrigin(string $page_site_origin): void
    {
        self::$page_site_origin = $page_site_origin;
    }

    /**
     * @return string Indica el estilo CSS al que pertenece la página
     */
    public static function getPageStyle(): string
    {
        return self::$page_style;
    }

    /**
     * @param string $page_style Indica el estilo CSS al que pertenece la página
     */
    public static function setPageStyle(string $page_style): void
    {
        self::$page_style = $page_style;
    }
}
