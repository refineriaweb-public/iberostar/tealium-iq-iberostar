<?php

namespace RefineriaWeb\TealiumIQIberostar\Traits;

/**
 * Trait UtagDataDevice
 * @package RefineriaWeb\TealiumIQIberostar\Traits
 *
 * El nombre de las variables se escribirá usando siempre minúsculas, estarán definidas en inglés
 * y en el caso de haber varias palabras, estarán concatenadas por un guion bajo.
 * El valor de las variables será también en inglés, en minúsculas a excepción de códigos ISO y códigos de Hotel,
 * que irán en mayúsculas y nombres de hotel que irán en el nombre original, minúsculas y sin acento.
 */
trait UtagDataDevice
{
    /** @var string Dispositivo de navegación por el que está entrando el usuario. Ejemplos: tablet, mobile, desktop */
    private static $device_type;

    /** @var string Ancho del dispositivo.  */
    private static $device_with;

    /** @var string Alto del dispositivo */
    private static $device_height;

    /** @var string Si cumple determinadas condiciones lanzar una de estas opciones (de tamaño) que provoca que se
     * visualice una u otra versión.
     * Posibles valores: desktop, mobile. independiente de la tipologia de dispositivo del usuario.
     * Dice qué versión se está mostrando al usuario.
     */
    private static $device_version;

    /**
     * @return string
     */
    public static function getDeviceType(): string
    {
        return self::$device_type;
    }

    /**
     * @param string $device_type
     */
    public static function setDeviceType(string $device_type): void
    {
        self::$device_type = $device_type;
    }

    /**
     * @return string
     */
    public static function getDeviceWith(): string
    {
        return self::$device_with;
    }

    /**
     * @param string $device_with
     */
    public static function setDeviceWith(string $device_with): void
    {
        self::$device_with = $device_with;
    }

    /**
     * @return string
     */
    public static function getDeviceHeight(): string
    {
        return self::$device_height;
    }

    /**
     * @param string $device_height
     */
    public static function setDeviceHeight(string $device_height): void
    {
        self::$device_height = $device_height;
    }

    /**
     * @return string
     */
    public static function getDeviceVersion(): string
    {
        return self::$device_version;
    }

    /**
     * @param string $device_version
     */
    public static function setDeviceVersion(string $device_version): void
    {
        self::$device_version = $device_version;
    }
}
