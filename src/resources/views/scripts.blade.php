@if(isset($utagData))
    <input type="hidden" value='{!! json_encode($utagData->utagData) !!}' class="js-utag-data-info" />
    <input type="hidden" value='{!! json_encode($utagData->productImpressionList) !!}' class="js-utag-data-product-impression-list" />
    <script src="{!! asset('vendor/refineriaweb/tealium-iq-iberostar/assets/js/blockadblock.js') !!}"></script>
    <script>
        const utag_data = JSON.parse(document.querySelector('.js-utag-data-info').value);
        const $fastBookingFormTealiumIQ = document.getElementsByClassName('js-fastbooking-form-tealium-iq');
        const $contactFormTealiumIQ = document.getElementsByClassName('js-contact-form-tealium-iq');
        let fastBookingFormInputs = [];
        let contactFormInputs = [];
        let contactFormStartTypping = [];

        utag_data.device_width = getDeviceWith();
        utag_data.device_height = getDevideHeight();
        utag_data.page_words_count = countWords();

        /* INI - EVENTS */
        window.addEventListener('load', function() {
            checkIfHasAdsBlocker();
            checkIfHasPromotions();
            addKruxInfo();
            addVisitorInfo();
            addUserInfo();
            setCookieForCRS();
            utagDataForceBooleanToString();
            productImpressionListLoadEvent();
        });

        for (let i in $fastBookingFormTealiumIQ) {
            let inputs = $fastBookingFormTealiumIQ[i];

            for (let j = 0; j < inputs.length; j++) {
                if ('undefined' !== typeof inputs[j]) {
                    // inputs[j].addEventListener('blur', fastBookingForm, false);
                    // inputs[j].addEventListener('change', fastBookingForm, false);
                    inputs[j].addEventListener('focusin', fastBookingForm, false);
                }
            }
        }

        for (let i = 0; i < $contactFormTealiumIQ.length; i++) {
            let inputs = $contactFormTealiumIQ[i];
            contactFormStartTypping[i] = false;

            for (let j = 0; j < inputs.length; j++) {
                if ('undefined' !== typeof inputs[j]) {
                    // inputs[j].addEventListener('blur', fastBookingForm, false);
                    // inputs[j].addEventListener('change', fastBookingForm, false);
                    inputs[j].addEventListener('focusin', function (event) {
                        let name = event.target.dataset.tealiumiq_formname;

                        if (typeof name !== "undefined"
                            && 0 > contactFormInputs.indexOf(name)
                            && !contactFormStartTypping[i]
                        ) {
                            contactFormInputs.push(name);
                            tealiumIQContactFormStart(name);
                            contactFormStartTypping[i] = true;
                        }
                    }, false);
                }
            }

            $contactFormTealiumIQ[i].addEventListener('submit', function () {
                tealiumIQContactFormValidate('.js-contact-form-tealium-iq');
            }, false);
        }
        /* END - EVENTS */

        /**
         * Get Device Widht
         * @returns {number} Device Width
         */
        function getDeviceWith() {
            return window.innerWidth;
        }

        /**
         * Get Device Height
         * @returns {number} Device Height
         */
        function getDevideHeight() {
            return window.innerHeight;
        }

        /**
         * Count Words
         * @returns {number} Words
         */
        function countWords() {
            let body = document.body;
            if (body) {
                let content = document.body[('innerText' in document.body) ? 'innerText' : 'textContent'];
                content = noscript(content);
                return content.match(/\S+/g).length;
            }
            return 0;
        }

        function noscript(strCode) {
            return strCode.replace(/<script.*?>.*?<\/script>/igm, '')
        }

        /**
         * Date Diff
         * @param {number} first - First date
         * @param {number} second - Second date
         * @return {number} Days
         */
        function datediff(first, second) {
            // Take the difference between the dates and divide by milliseconds per day.
            // Round to nearest whole number to deal with DST.
            return Math.round((second-first)/(1000*60*60*24));
        }

        /**
         * Make ID
         * @param {number} length - Lenght
         * @param {boolean} numbers - Allow numbers?
         * @param {boolean} apperLetters - Allow uppers letters?
         * @param {boolean} lowerLetters - Allow lowers letters?
         * @return {string} ramdom string
         */
        function makeId(length = 20, numbers = true, apperLetters = true, lowerLetters = true) {
            const numbersPossible = "0123456789";
            const apperLettersPossible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const lowerLettersPossible = "abcdefghijklmnopqrstuvwxyz";
            let text = "";
            let possible = "";

            possible += (true === numbers) ? numbersPossible : "";
            possible += (true === apperLetters) ? apperLettersPossible : "";
            possible += (true === lowerLetters) ? lowerLettersPossible : "";

            for (let i = 0; i < length; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }

            return text;
        }

        /**
         * Make Visitor Session ID
         * @return {string} ramdom string
         */
        function makeVisitorSessionId() {
            return makeId(16,true,false,true);
        }


        /**
         * Get Cookie Value
         * @param {string} cookieName - Cookie Name
         * @returns {string} Cookie Value
         */
        function getCookieValue(cookieName) {
            let value = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
            return value ? value.pop() : '';
        }

        /**
         * Set Cookie Value
         */
        function setCookie(cookieName, cookieValue, days) {
            let date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            let expires = "expires="+ date.toUTCString();
            let domain = location.host.indexOf('www.') && location.host || location.host.replace('www.', '.');

            if (domain.indexOf('iberostar.com') > 0) {
                domain = 'iberostar.com';
            }

            document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/;domain=" + domain;
        }

        /**
         * Check If Has Ads Blocker
         */
        function checkIfHasAdsBlocker() {
            if (typeof blockAdBlock === 'undefined') {
                utag_data.page_has_adsblocker = "true";
            } else {
                blockAdBlock.onDetected(function () {
                    utag_data.page_has_adsblocker = "true";
                });

                blockAdBlock.onNotDetected(function () {
                    utag_data.page_has_adsblocker = "false";
                });
            }
        }

        /**
         * Check if page has promotions
         */
        function checkIfHasPromotions() {
            let promotions = document.querySelectorAll('[data-tealiumiq_promotion_id]');

            if (0 === promotions.length) return;

            let promotionsToAdd = {
                enh_action: "addPromo",
                promotion_id: [],
                promotion_name: [],
                promotion_creative: [],
                promotion_position: []
            };

            for (let i = 0; i < promotions.length; i++) {
                promotions[i].addEventListener('mousedown', function (e) {
                    tealiumIQPromotionEventClick(this);
                });

                promotionsToAdd.promotion_id.push(promotions[i].dataset.tealiumiq_promotion_id);
                promotionsToAdd.promotion_name.push(promotions[i].dataset.tealiumiq_promotion_name);
                promotionsToAdd.promotion_creative.push(promotions[i].dataset.tealiumiq_promotion_creative);
                promotionsToAdd.promotion_position.push(promotions[i].dataset.tealiumiq_promotion_position);
            }

            tealiumIQPromotionAdd(promotionsToAdd);
        }

        /**
         * Add Krux Info
         */
        function addKruxInfo() {
            if (typeof(Storage) !== "undefined") {
                utag_data.krux_id = localStorage.kxiberostar_kuid;
                utag_data.krux_segments = localStorage.kxiberostar_segs;
            }
            if (typeof(utag_data.krux_id) === "undefined") {
                delete utag_data.krux_id;
            }
            if (typeof(utag_data.krux_segments) === "undefined") {
                delete utag_data.krux_segments;
            }
        }

        /**
         * Add Visitor Info
         */
        function addVisitorInfo() {
            const days = 365;
            let date = new Date();
            let isRepeater = true;
            let recurrence = 0;
            let sessionCount = 0;


            if ("" === getCookieValue("tealium_iq_visitor_session_id")) {
                const ramdomID = makeVisitorSessionId();
                isRepeater = false;

                setCookie('tealium_iq_visitor_session_id', ramdomID, days);
            } else {
                sessionCount = parseInt(getCookieValue("tealium_iq_visitor_session_count")) + 1;
                recurrence = Math.abs(datediff(date.getTime(), parseInt(getCookieValue("tealium_iq_visitor_last_visit"))));
            }

            setCookie('tealium_iq_visitor_session_count', sessionCount, days);
            setCookie('tealium_iq_visitor_is_repeater', isRepeater, days);
            setCookie('tealium_iq_visitor_recurrence', recurrence, days);
            setCookie('tealium_iq_visitor_last_visit', date.getTime(), days);

            utag_data.visitor_id = getCookieValue("_ga");
            utag_data.visitor_session_id = getCookieValue("tealium_iq_visitor_session_id");
            utag_data.visitor_is_repeater = ("true" === getCookieValue("tealium_iq_visitor_is_repeater"));
            utag_data.visitor_recurrence = parseInt(getCookieValue("tealium_iq_visitor_recurrence"));
            utag_data.visitor_session_count = parseInt(getCookieValue("tealium_iq_visitor_session_count"));

            // Set to false
            utag_data.visitor_strategy_market = "false";
            utag_data.visitor_personalized_tratement = "false";
            utag_data.visitor_recive_advertising = "false";
        }

        /**
         * Add User Info
         */
        function addUserInfo() {
            utag_data.user_level = "level 0";
        }

        /**
         * Set Cookie For CRS
         */
        function setCookieForCRS() {
            const days = 365;
            setCookie('campaign_content', utag_data.campaign_content, days);
            setCookie('campaign_cp', utag_data.campaign_cp, days);
            setCookie('campaign_medium', utag_data.campaign_medium, days);
            setCookie('campaign_name', utag_data.campaign_name, days);
            setCookie('campaign_source', utag_data.campaign_source, days);
            setCookie('campaign_term', utag_data.campaign_term, days);
            setCookie('hotel_headquarter_origin', utag_data.hotel_headquarter_origin, days);
            setCookie('page_site_origin', utag_data.page_site_origin, days);
            setCookie('visitor_id', utag_data.visitor_id, days);
            // setCookie('visitor_internal_market', utag_data.visitor_internal_market, days);
            setCookie('visitor_is_repeater', utag_data.visitor_is_repeater, days);
            setCookie('visitor_recurrence', utag_data.visitor_recurrence, days);
            setCookie('visitor_session_count', utag_data.visitor_session_count, days);
            setCookie('visitor_session_id', utag_data.visitor_session_id, days);
            // setCookie('visitor_strategy_market', utag_data.visitor_strategy_market, days);
            setCookie('visitor_type', utag_data.visitor_type, days);
        }

        /**
         * Tealium IQ Promotion Add
         * @param {Object} promotionsToAdd - Promotion To Add
         * @param {string} promotionsToAdd.enh_action
         * @param {string} promotionsToAdd.promotion_id - Promotion Id
         * @param {string} promotionsToAdd.promotion_name - Promotion Name
         * @param {string} promotionsToAdd.promotion_creative - Promotion Creative
         * @param {string} promotionsToAdd.promotion_position - Promotion Position
         */
        function tealiumIQPromotionAdd(promotionsToAdd) {
            utag_data.enh_action =  promotionsToAdd.enh_action;
            utag_data.promotion_id =  promotionsToAdd.promotion_id;
            utag_data.promotion_name =  promotionsToAdd.promotion_name;
            utag_data.promotion_creative =  promotionsToAdd.promotion_creative;
            utag_data.promotion_position =  promotionsToAdd.promotion_position;
        }

        /**
         * Tealium IQ Promotion Event Click
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         * @param {string} item.dataset.tealiumiq_promotion_id - Promotion Id
         * @param {string} item.dataset.tealiumiq_promotion_name - Promotion Name
         * @param {string} item.dataset.tealiumiq_promotion_creative - Promotion Creative
         * @param {string} item.dataset.tealiumiq_promotion_position - Promotion Position
         */
        function tealiumIQPromotionEventClick(item) {
            if (typeof utag === 'undefined') return;
            utag.link( {
                enh_action: "promo_click",
                event_cat: item.dataset.tealiumiq_event_cat,
                event_act: "promo_click",
                event_lbl: item.dataset.tealiumiq_event_lbl,
                promotion_id: [item.dataset.tealiumiq_promotion_id],
                promotion_name: [item.dataset.tealiumiq_promotion_name],
                promotion_creative: [item.dataset.tealiumiq_promotion_creative],
                promotion_position: [item.dataset.tealiumiq_promotion_position]
            });
        }

        /**
         * Tealium IQ Product Event Click
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_enh_action - Enh Action
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_act - Event Action
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         * @param {string} item.dataset.tealiumiq_product_id - Product Id
         * @param {string} item.dataset.tealiumiq_product_name - Product Name
         * @param {string} item.dataset.tealiumiq_product_category - Product Category
         * @param {string} item.dataset.tealiumiq_product_variant - Product Variant
         * @param {string} item.dataset.tealiumiq_product_brand - Product Brand
         * @param {string} item.dataset.tealiumiq_product_action_list - Product Action List
         * @param {string} item.dataset.tealiumiq_product_position - Product Position
         * @param {string} item.dataset.tealiumiq_product_unit_price - Product Unit Price
         */
        function tealiumIQProductEventClick(item) {
            let params = {
                enh_action: item.dataset.tealiumiq_enh_action || "product_click",
                event_cat: item.dataset.tealiumiq_event_cat || "ecommerce",
                event_act: item.dataset.tealiumiq_event_act || "promo_click",
                event_lbl: item.dataset.tealiumiq_event_lbl,
                product_id: item.dataset.tealiumiq_product_id,
                product_name: item.dataset.tealiumiq_product_name,
                product_category: item.dataset.tealiumiq_product_category,
                product_variant: [item.dataset.tealiumiq_product_variant],
                product_brand: item.dataset.tealiumiq_product_brand,
                product_action_list: item.dataset.tealiumiq_product_action_list,
                product_position: [item.dataset.tealiumiq_product_position],
                product_unit_price: item.dataset.tealiumiq_product_unit_price,
            };
            console.log({"TealiumIQ Product Event Click" : params});

            if (typeof utag === 'undefined') return;
            utag.link(params);
        }

        /**
         * Tealium IQ Call Center Event Click
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_act - Event Action
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         */
        function tealiumIQCallCenterEventClick(item) {
            console.log({"TealiumIQ Call Center Event Click" : item});
            if (typeof utag === 'undefined') return;
            utag.link( {
                event_cat: item.dataset.tealiumiq_event_cat,
                event_act: item.dataset.tealiumiq_event_act,
                event_lbl: item.dataset.tealiumiq_event_lbl
            });
        }

        /**
         * Tealium IQ Newsletter Event Click
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_act - Event Action
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         */
        function tealiumIQNewsletterEventClick(item) {
            console.log({"TealiumIQ Newsletter Event Click" : item});
            if (typeof utag === 'undefined') return;
            utag.link( {
                event_cat: item.dataset.tealiumiq_event_cat,
                event_act: item.dataset.tealiumiq_event_act,
                event_lbl: item.dataset.tealiumiq_event_lbl
            });
        }

        /**
         * Tealium IQ Newsletter Event Success
         */
        function tealiumIQNewsletterEventSuccess() {
            let item = {
                dataset : {
                    tealiumiq_event_cat : 'newsletter',
                    tealiumiq_event_act : 'user suscribed',
                    tealiumiq_event_lbl : ''
                }
            };
            console.log({"TealiumIQ Newsletter Event Success" : item});
            if (typeof utag === 'undefined') return;
            tealiumIQChangePageInfo(item);
        }

        /**
         * Tealium IQ Newsletter Event Error
         */
        function tealiumIQNewsletterEventError() {
            let item = {
                dataset : {
                    tealiumiq_event_cat : 'newsletter',
                    tealiumiq_event_act : 'error user suscribed',
                    tealiumiq_event_lbl : 'alr'
                }
            };
            console.log({"TealiumIQ Newsletter Event Error" : item});
            if (typeof utag === 'undefined') return;
            tealiumIQChangePageInfo(item);
        }

        /**
         * Tealium IQ Newsletter Event Conditions Error
         */
        function tealiumIQNewsletterEventConditionsError() {
            let item = {
                dataset : {
                    tealiumiq_event_cat : 'newsletter',
                    tealiumiq_event_act : 'error user suscribed',
                    tealiumiq_event_lbl : 'term'
                }
            };
            console.log({"TealiumIQ Newsletter Event Conditions Error" : item});
            if (typeof utag === 'undefined') return;
            tealiumIQChangePageInfo(item);
        }

        /**
         * Tealium IQ Change Language
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_act - Event Action
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         */
        function tealiumIQChangeLanguageEvent(item) {
            console.log({"TealiumIQ Change Language" : item});
            if (typeof utag === 'undefined') return;
            tealiumIQChangePageInfo(item);
        }

        /**
         * Tealium IQ Book Now Event Click
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_act - Event Action
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         */
        function tealiumIQBookNowEventClick(item) {
            console.log({"TealiumIQ Book Now Event Click" : item});
            if (typeof utag === 'undefined') return;
            tealiumIQChangePageInfo(item);
        }

        /**
         * Tealium IQ Change Page Info
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_act - Event Action
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         */
        function tealiumIQChangePageInfo(item) {
            console.log({"TealiumIQ Change Page Info Event" : item});
            if (typeof utag === 'undefined') return;
            utag.link( {
                event_cat: item.dataset.tealiumiq_event_cat,
                event_act: item.dataset.tealiumiq_event_act,
                event_lbl: item.dataset.tealiumiq_event_lbl
            });
        }

        /**
         * Tealium IQ Event
         * @param {Object} item
         * @param {string} item.dataset.tealiumiq_event_cat - Event Category
         * @param {string} item.dataset.tealiumiq_event_act - Event Action
         * @param {string} item.dataset.tealiumiq_event_lbl - Event Label
         */
        function tealiumIQEvent(item) {
            tealiumIQChangePageInfo(item);
        }

        /**
         * Tealium IQ Contact Form Validate
         * @param contactForm - Contact Form
         */
        function tealiumIQContactFormValidate(contactForm) {
            console.log({"TealiumIQ" : "Form Validate"});

            let inputsInvalid = document.querySelectorAll(contactForm + ' :invalid');

            if (0 === inputsInvalid.length) {
                return;
            }

            let errorFields = [];

            for (let i = 0; i < inputsInvalid.length; i++) {
                if (!isHidden(inputsInvalid[i])) {
                    if (typeof inputsInvalid[i].dataset.tealiumiq_formname !== 'undefined') {
                        errorFields.push(inputsInvalid[i].dataset.tealiumiq_formname);
                    }
                }
            }

            let errorFieldsName = errorFields.join('-');

            console.log(errorFieldsName);

            let event = {
                "event_cat": "form",
                "event_act": "error form-contact",
                "event_lbl": errorFields.join('-')
            };

            tealiumIQContactFormError(event);
        }

        /**
         * Tealium IQ Contact Form Start Typping
         * @param {string} fieldName - Field Name
         */
        function tealiumIQContactFormStart(fieldName) {
            let event = {
                "event_cat": "form",
                "event_act": "start form-contact",
                "event_lbl": fieldName
            };
            console.log({"TealiumIQ Form Start" : event});
            if (typeof utag === 'undefined') return;
            utag.link(event);
        }

        /**
         * Tealium IQ Contact Form Send
         */
        function tealiumIQContactFormSend() {
            console.log({"TealiumIQ" : "Form Send"});
            let event = {
                "event_cat": "form",
                "event_act": "try send form-contact",
                "event_lbl": ""
            };
            if (typeof utag === 'undefined') return;
            utag.link(event);
        }

        /**
         * Tealium IQ Contact Form Success
         */
        function tealiumIQContactFormSuccess() {
            let event = {
                "event_cat": "form",
                "event_act": "success form-contact",
                "event_lbl": ""
            };
            console.log({"TealiumIQ Form Success" : event});
            if (typeof utag === 'undefined') return;
            utag.link(event);
        }

        /**
         * Tealium IQ Contact Form Error
         * @param {Object} event - Event
         */
        function tealiumIQContactFormError(event) {
            console.log({"TealiumIQ Form Error" : event});
            if (typeof utag === 'undefined') return;
            utag.link(event);
        }

        /**
         * Tealium IQ Multimedia Click
         * @param {string} category - Category
         * @param {string} action - Action
         * @param {string} label - Label
         */
        function tealiumIQMultimediaClick(category, action, label) {
            category = (undefined === category) ? "hotel information" : category;
            action = (undefined === action) ? "see multimedia info" : action;
            label = (undefined === label) ? "photos" : label;
            let type = null;

            switch (label) {
                case "photos": type = "page_images_clicked"; break;
                case "videos": type = "page_videos_clicked"; break;
                case "360": type = "page_videos_360_clicked"; break;
            }

            let event = {
                "event_cat": category,
                "event_act": action,
                "event_lbl": label,
                [type]: 1
            };
            console.log({"TealiumIQ Photo Click" : event});

            if (typeof utag === 'undefined') return;
            utag.link(event);
        }

        /**
         * Tealium IQ Searcher Input Change
         * @param {string} category - Category
         * @param {string} action - Action
         * @param {string} label - Label
         */
        function tealiumIQSearcherInputChange(category, action, label) {
            category = (undefined === category) ? "searcher" : category;
            action = (undefined === action) ? "dates" : action;
            label = (undefined === label) ? "" : label;
            let event = {
                "event_cat": category,
                "event_act": action,
                "event_lbl": label
            };
            console.log({"TealiumIQ Searcher Input Change": event});

            if (typeof utag === 'undefined') return;
            utag.link(event);
        }

        /**
         * Fast Bookin From
         * @param input - Event Input
         */
        function fastBookingForm(input) {
            let name = input.target.dataset.tealiumiq_formname;

            if (typeof name !== "undefined" && 0 > fastBookingFormInputs.indexOf(name)) {
                fastBookingFormInputs.push(name);
                tealiumIQSearcherInputChange("searcher", name, "");
            }
        }

        /**
         * Product Impression List Load Event
         */
        function productImpressionListLoadEvent() {
            if (typeof utag === 'undefined') return;

            const utagDataProductImpressionList = JSON.parse(document.querySelector('.js-utag-data-product-impression-list').value);

            for (let i in utagDataProductImpressionList) {
                let item = utagDataProductImpressionList[i];

                utag.link( {
                    event_cat: "ecommerce",
                    event_act: "product impression list",
                    event_lbl: "",
                    non_interaction: 1,
                    product_impression_id: item.product_impression_id,
                    product_impression_name: item.product_impression_name,
                    product_impression_brand: item.product_impression_brand,
                    product_impression_variant: item.product_impression_variant,
                    product_impression_category: item.product_impression_category,
                    product_impression_list: item.product_impression_list,
                    product_impression_price: item.product_impression_price,
                    product_impression_position: item.product_impression_position,
                });
            }
        }

        /**
         * Is Hidden
         * @param el
         * @returns {boolean}
         */
        function isHidden(el) {
            let t1 = el.currentStyle ? el.currentStyle.visibility : getComputedStyle(el, null).visibility;
            let t2 = el.currentStyle ? el.currentStyle.display : getComputedStyle(el, null).display;
            if (t1 === "hidden" || t2 === "none") {
                return true;
            }
            while (!(/body/i).test(el)) {
                el = el.parentNode;
                t1 = el.currentStyle ? el.currentStyle.visibility : getComputedStyle(el, null).visibility;
                t2 = el.currentStyle ? el.currentStyle.display : getComputedStyle(el, null).display;
                if (t1 === "hidden" || t2 === "none") {
                    return true;
                }
            }
            return false;
        }

        /**
         * Boolean To String
         */
        function utagDataForceBooleanToString() {
            for (let i in utag_data) {
                if ("boolean" === typeof utag_data[i]) {
                    utag_data[i] = utag_data[i].toString();
                }
            }
        }
    </script>

    @if(App::environment() !== 'local')
        <script type="text/javascript">
            (function(a,b,c,d){
                a='//tags.tiqcdn.com/utag/iberostar/main/{!! $utagData->environment !!}/utag.js';
                b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
                a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
            })();
        </script>
    @endif
@endif
