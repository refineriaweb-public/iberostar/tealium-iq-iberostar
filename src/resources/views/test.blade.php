<h1>Test UTAG Info</h1>
<h2>Environment: {!! $utagData->environment !!}</h2>
@include('TealiumIQIberostar::scripts')

<div id='debugArea'></div>

<script>
    const container = document.getElementById('debugArea');
    container.innerHTML = print_r(JSON.parse('{!! json_encode($utagData) !!}'));

    function print_r(o) {
        return JSON.stringify(o,null,'\t')
            .replace(/\n/g,'<br>')
            .replace(/\t/g,'&nbsp;&nbsp;&nbsp;');
    }
</script>
