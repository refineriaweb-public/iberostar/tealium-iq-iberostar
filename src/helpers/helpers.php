<?php

if (!function_exists('tealiumIQPromotionInfo')) {
    /**
     * Tealium IQ Promotion Info
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_promotion_id'   => (string) ["<id de la promoción>"]
     *      'tealiumiq_promotion_name'   => (string) ["<nombre de la promoción>"]
     *      'tealiumiq_promotion_creative'   => (string) ["<creatividad de la promoción>"]
     *      'tealiumiq_promotion_position' => (string) ["<posición promoción>"]
     *      'tealiumiq_event_cat' => (string)
     *      'tealiumiq_event_lbl' => (string)
     *    ]
     * @return string
     */
    function tealiumIQPromotionInfo(array $params = [])
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQPromotionInfo($params);
    }
}

if (!function_exists('tealiumIQEventCallCenter')) {
    /**
     * Tealium IQ Event Call Center Click
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_event_cat'   => (string) "call center"
     *      'tealiumiq_event_act'   => (string) "<action_type_call_center>". click contact button|call me back|click to call
     *      'tealiumiq_event_lbl'   => (string) "<location_button>". pop up, header, footer, sticky
     *    ]
     * @return string
     */
    function tealiumIQEventCallCenter(array $params = [])
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQEventCallCenter($params);
    }
}

if (!function_exists('tealiumIQEventNewsletter')) {
    /**
     * Tealium IQ Event Newsletter Submit
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_event_cat'   => (string) "newsletter"
     *      'tealiumiq_event_act'   => (string) "user suscribed"
     *      'tealiumiq_event_lbl'   => (string) ""
     *    ]
     * @return string
     */
    function tealiumIQEventNewsletter(array $params = [])
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQEventNewsletter($params);
    }
}

if (!function_exists('tealiumIQEventChangeLang')) {
    /**
     * Tealium IQ Event Change Lang
     * @param string $lang Lang code
     * @return string
     */
    function tealiumIQEventChangeLang(string $lang)
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQEventChangeLang($lang);
    }
}

if (!function_exists('tealiumIQEventBookNow')) {
    /**
     * Tealium IQ Event Change Lang
     * @param string $roomName Room Name
     * @return string
     */
    function tealiumIQEventBookNow(string $roomName)
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQEventBookNow($roomName);
    }
}

if (!function_exists('tealiumIQEvent')) {
    /**
     * Tealium IQ Event
     * @param string $category Category
     *  Examples:
     *  "adsblocker"|"affiliates"|"availability options"|"booking quote"|"call center"|"checkin online"|
     *  "change page info"|"check availability"|"compare hotels"|"compare rooms"|"destinations"|"downloads"|
     *  "ecommerce"|"filters destinations"|"form"|"hotel information"|"jobs Iberostar"|"last search"|"lead"|
     *  "login"|"manage my booking"|"my iberostar gift"|"my room online"|"newsletter"|"not availability"|
     *  "offers"|"request reservation"|"rfp"|"searcher"|"search"|"social events"|
     * @param string $action Action
     * @param string|null $label Label
     * @return string
     */
    function tealiumIQEvent(string $category, string $action, string $label = null)
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQEvent($category, $action, $label);
    }
}


if (!function_exists('tealiumIQEventMultimediaClick')) {
    /**
     * Tealium IQ Event Multimedia Click
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_event_cat' => (string)
     *      'tealiumiq_event_act' => (string)
     *      'tealiumiq_event_lbl' => (string)
     *    ]
     * @return string
     */
    function tealiumIQEventMultimediaClick(array $params = [])
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQEventMultimediaClick($params);
    }
}

if (!function_exists('tealiumIQEventProductClick')) {
    /**
     * Tealium IQ Event Product Click
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_enh_action' => (string) "product_click"
     *      'tealiumiq_event_cat' => (string) "ecommerce"
     *      'tealiumiq_event_act' => (string) "product_click"
     *      'tealiumiq_event_lbl' => (string) "<product name>"
     *      'tealiumiq_product_id' => (string) "<product id>"
     *      'tealiumiq_product_name' => (string) "<product name>"
     *      'tealiumiq_product_category' => (string) "<product category>"
     *      'tealiumiq_product_variant' => (string) "<variant>"
     *      'tealiumiq_product_brand' => (string) "<product brand>"
     *      'tealiumiq_product_action_list' => (string) "<lista en la que se encontraba el producto>"
     *      'tealiumiq_product_position' => (array) "[<position>]"
     *      'tealiumiq_product_unit_price' => string) "<product_unit_price>"
     *    ]
     * @return string
     */
    function tealiumIQEventProductClick(array $params = [])
    {
        return \RefineriaWeb\TealiumIQIberostar\Classes\Helpers::tealiumIQEventProductClick($params);
    }
}
