<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use Illuminate\Support\Facades\Config;

/**
 * Class UtagDataProduct
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataProduct
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataProduct;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params = [])
    {
        self::setDefaultProductInfo();

        $output = self::getDefaultProductInfo();

        $output = array_merge($output, self::checkParams($params));

        return $output;
    }

    /**
     * Check and Validate Params
     * @param $params
     * @return array
     */
    public static function checkParams($params) : array
    {
        $output = [];

        if (isset($params['product_market']) && strlen($params['product_market']) > 0) {
            $output['product_market'] = $params['product_market'];
        }

        return $output;
    }

    /**
     * Set Default Krux Info
     */
    private static function setDefaultProductInfo() : void
    {
        self::setProductMarket(Config::get('iberostar-tealium-iq.destination.destination_country', ''));
    }

    /**
     * Get Default Krux Info
     * @return array
     */
    private static function getDefaultProductInfo() : array
    {
        $output = [];

        if (strlen(self::getProductMarket()) > 0) {
            $output['product_market'] = self::getProductMarket();
        }

        return $output;
    }
}
