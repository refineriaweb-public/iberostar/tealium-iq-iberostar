<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use Illuminate\Support\Facades\Config;

/**
 * Class UtagDataKrux
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataKrux
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataKrux;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params = [])
    {
        self::setDefaultKruxInfo();

        $output = self::getDefaultKruxInfo();

        $output = array_merge($output, self::checkParams($params));

        return $output;
    }

    /**
     * Check and Validate Params
     * @param $params
     * @return array
     */
    public static function checkParams($params) : array
    {
        $output = [];

        if (isset($params['krux_id']) && strlen($params['krux_id']) > 0) {
            $output['krux_id'] = $params['krux_id'];
        }

        if (isset($params['krux_segments']) && strlen($params['krux_segments']) > 0) {
            $output['krux_segments'] = $params['krux_segments'];
        }

        return $output;
    }

    /**
     * Set Default Krux Info
     */
    private static function setDefaultKruxInfo() : void
    {
        self::setKruxId(Config::get('iberostar-tealium-iq.krux.krux_id', ''));
        self::setKruxSegments(Config::get('iberostar-tealium-iq.krux.krux_segments', ''));
    }

    /**
     * Get Default Krux Info
     * @return array
     */
    private static function getDefaultKruxInfo() : array
    {
        $output = [];

        if (strlen(self::getKruxId()) > 0) {
            $output['krux_id'] = self::getKruxId();
        }

        if (strlen(self::getKruxSegments()) > 0) {
            $output['krux_segments'] = self::getKruxSegments();
        }

        return $output;
    }
}
