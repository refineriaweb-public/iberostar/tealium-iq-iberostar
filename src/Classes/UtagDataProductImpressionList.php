<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

/**
 * Class UtagDataProductImpressionList
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataProductImpressionList
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataProductImpressionList;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params)
    {
        $output = [];
        $productImpressionListArray = isset($params['product_impression_list']) ? $params['product_impression_list'] : [];

        foreach ($productImpressionListArray as $productImpressionList) {
            self::setParams($productImpressionList);

            array_push($output, self::getParams());

            self::cleanParams();
        }

        return $output;
    }

    /**
     * Clean Params
     */
    public static function cleanParams()
    {
        UtagDataProductImpressionList::setProductImpressionList([]);
        UtagDataProductImpressionList::setProductImpressionBrand([]);
        UtagDataProductImpressionList::setProductImpressionId([]);
        UtagDataProductImpressionList::setProductImpressionName([]);
        UtagDataProductImpressionList::setProductImpressionCategory([]);
        UtagDataProductImpressionList::setProductImpressionPosition([]);
        UtagDataProductImpressionList::setProductImpressionPrice([]);
        UtagDataProductImpressionList::setProductImpressionVariant([]);
    }

    /**
     * Get Params
     * @return array
     */
    public static function getParams() : array
    {
        return [
            'product_impression_list' => UtagDataProductImpressionList::getProductImpressionList(),
            'product_impression_brand' => UtagDataProductImpressionList::getProductImpressionBrand(),
            'product_impression_id' => UtagDataProductImpressionList::getProductImpressionId(),
            'product_impression_name' => UtagDataProductImpressionList::getProductImpressionName(),
            'product_impression_category' => UtagDataProductImpressionList::getProductImpressionCategory(),
            'product_impression_position' => UtagDataProductImpressionList::getProductImpressionPosition(),
            'product_impression_price' => UtagDataProductImpressionList::getProductImpressionPrice(),
            'product_impression_variant' => UtagDataProductImpressionList::getProductImpressionVariant(),
        ];
    }

    /**
     * Set Params
     * @param array $params
     */
    public static function setParams(array $params) : void
    {
        UtagDataProductImpressionList::setProductImpressionList($params['product_impression_list']);
        UtagDataProductImpressionList::setProductImpressionBrand($params['product_impression_brand']);
        UtagDataProductImpressionList::setProductImpressionId($params['product_impression_id']);
        UtagDataProductImpressionList::setProductImpressionName($params['product_impression_name']);
        UtagDataProductImpressionList::setProductImpressionCategory($params['product_impression_category']);
        UtagDataProductImpressionList::setProductImpressionPosition($params['product_impression_position']);
        UtagDataProductImpressionList::setProductImpressionPrice($params['product_impression_price']);
        UtagDataProductImpressionList::setProductImpressionVariant($params['product_impression_variant']);
    }
}
