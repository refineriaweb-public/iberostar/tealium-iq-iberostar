<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use Illuminate\Support\Facades\Config;

/**
 * Class UtagDataHotel
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataHotel
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataHotel;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params = []) : array
    {
        self::setDefaultHotelInfo();
        self::checkParams($params);
        $output = self::getDefaultHotelInfo();

        return $output;
    }

    /**
     * Check and Validate Params
     * @param $params
     */
    public static function checkParams($params) : void
    {
        if (isset($params['hotel_room_code']) && count($params['hotel_room_code']) > 0) {
            self::setHotelRoomCode($params['hotel_room_code']);
        }

        if (isset($params['hotel_room_name']) && count($params['hotel_room_name']) > 0) {
            self::setHotelRoomName($params['hotel_room_name']);
        }

        if (isset($params['hotel_room_category']) && count($params['hotel_room_category']) > 0) {
            self::setHotelRoomCategory($params['hotel_room_category']);
        }

        if (isset($params['hotel_headquarter_origin']) && strlen($params['hotel_headquarter_origin']) > 0) {
            self::setHotelHeadquarterOrigin($params['hotel_headquarter_origin']);
        }

        if (isset($params['hotel_headquarter_final']) && strlen($params['hotel_headquarter_final']) > 0) {
            self::setHotelHeadquarterFinal($params['hotel_headquarter_final']);
        }

        if (isset($params['hotel_sku']) && strlen($params['hotel_sku']) > 0) {
            self::setHotelSku($params['hotel_sku']);
        }

        if (isset($params['hotel_name']) && strlen($params['hotel_name']) > 0) {
            self::setHotelName($params['hotel_name']);
        }

        if (isset($params['hotel_short_name']) && strlen($params['hotel_short_name']) > 0) {
            self::setHotelShortName($params['hotel_short_name']);
        }

        if (isset($params['hotel_type']) && strlen($params['hotel_type']) > 0) {
            self::setHotelType($params['hotel_type']);
        }

        if (isset($params['hotel_is_only_adults'])) {
            self::setHotelIsOnlyAdults($params['hotel_is_only_adults']);
        }

        if (isset($params['hotel_stars']) && strlen($params['hotel_stars']) > 0) {
            self::setHotelStars($params['hotel_stars']);
        }

        if (isset($params['hotel_segment']) && strlen($params['hotel_segment']) > 0) {
            self::setHotelSegment($params['hotel_segment']);
        }

        if (isset($params['hotel_subsegment']) && strlen($params['hotel_subsegment']) > 0) {
            self::setHotelSubsegment($params['hotel_subsegment']);
        }

        if (isset($params['hotel_brand']) && strlen($params['hotel_brand']) > 0) {
            self::setHotelBrand($params['hotel_brand']);
        }

        if (isset($params['hotel_services']) && strlen($params['hotel_services']) > 0) {
            self::setHotelServices($params['hotel_services']);
        }
    }

    /**
     * Set Default Hotel Info
     */
    private static function setDefaultHotelInfo() : void
    {
        self::setHotelHeadquarterOrigin(Config::get('iberostar-tealium-iq.hotel.hotel_headquarter_origin', ''));
        self::setHotelHeadquarterFinal(Config::get('iberostar-tealium-iq.hotel.hotel_headquarter_final', ''));
        self::setHotelSku(Config::get('iberostar-tealium-iq.hotel.hotel_sku', ''));
        self::setHotelName(Config::get('iberostar-tealium-iq.hotel.hotel_name', ''));
        self::setHotelShortName(Config::get('iberostar-tealium-iq.hotel.hotel_short_name', ''));
        self::setHotelType(Config::get('iberostar-tealium-iq.hotel.hotel_type', ''));
        self::setHotelIsOnlyAdults(Config::get('iberostar-tealium-iq.hotel.hotel_is_only_adults', false));
        self::setHotelStars(Config::get('iberostar-tealium-iq.hotel.hotel_stars', ''));
        self::setHotelSegment(Config::get('iberostar-tealium-iq.hotel.hotel_segment', ''));
        self::setHotelSubsegment(Config::get('iberostar-tealium-iq.hotel.hotel_subsegment', ''));
        self::setHotelBrand(Config::get('iberostar-tealium-iq.hotel.hotel_brand', ''));
        self::setHotelServices(Config::get('iberostar-tealium-iq.hotel.hotel_services', ''));
    }

    /**
     * Get Default Hotel Info
     * @return array
     */
    private static function getDefaultHotelInfo() : array
    {
        $output = [];

        if (strlen(self::getHotelHeadquarterOrigin()) > 0) {
            $output['hotel_headquarter_origin'] = self::getHotelHeadquarterOrigin();
        }

        if (strlen(self::getHotelHeadquarterFinal()) > 0) {
            $output['hotel_headquarter_final'] = self::getHotelHeadquarterFinal();
        }

        if (strlen(self::getHotelSku()) > 0) {
            $output['hotel_sku'] = self::getHotelSku();
        }

        if (strlen(self::getHotelName()) > 0) {
            $output['hotel_name'] = self::getHotelName();
        }

        if (strlen(self::getHotelShortName()) > 0) {
            $output['hotel_short_name'] = self::getHotelShortName();
        }

        if (strlen(self::getHotelType()) > 0) {
            $output['hotel_type'] = self::getHotelType();
        }

        $output['hotel_is_only_adults'] = self::isHotelIsOnlyAdults();

        if (strlen(self::getHotelStars()) > 0) {
            $output['hotel_stars'] = self::getHotelStars();
        }

        if (strlen(self::getHotelSegment()) > 0) {
            $output['hotel_segment'] = self::getHotelSegment();
        }

        if (strlen(self::getHotelSubsegment()) > 0) {
            $output['hotel_subsegment'] = self::getHotelSubsegment();
        }

        if (strlen(self::getHotelBrand()) > 0) {
            $output['hotel_brand'] = self::getHotelBrand();
        }

        if (strlen(self::getHotelServices()) > 0) {
            $output['hotel_services'] = self::getHotelServices();
        }

        if (!is_null(self::getHotelRoomCode()) && count(self::getHotelRoomCode()) > 0) {
            $output['hotel_room_code'] = self::getHotelRoomCode();
        }

        if (!is_null(self::getHotelRoomName()) && count(self::getHotelRoomName()) > 0) {
            $output['hotel_room_name'] = self::getHotelRoomName();
        }

        if (!is_null(self::getHotelRoomCategory()) && count(self::getHotelRoomCategory()) > 0) {
            $output['hotel_room_category'] = self::getHotelRoomCategory();
        }

        return $output;
    }
}
