<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use Jenssegers\Agent\Agent;

/**
 * Class UtagDataDevice
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataDevice
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataDevice;

    /**
     * Get Info
     * @return array
     */
    public static function getInfo()
    {
        $output = [];

        self::checkDevideType();
        self::setDeviceVersion('desktop');

        $output['device_type'] = self::getDeviceType();
        $output['device_version'] = self::getDeviceVersion();

        return $output;
    }

    /**
     * Check Device Type
     */
    public static function checkDevideType()
    {
        $agent = new Agent();

        if ($agent->isMobile()) {
            if ($agent->isTablet()) {
                $deviceType = 'tablet';
            } else {
                $deviceType = 'mobile';
            }
        } else {
            $deviceType = 'desktop';
        }

        UtagDataDevice::setDeviceType($deviceType);
    }
}
