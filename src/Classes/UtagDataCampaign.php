<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

/**
 * Class UtagDataCampaign
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataCampaign
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataCampaign;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params = [])
    {
        $inputs = isset($params['request']) ? $params['request']->all() : [];

        self::setDefaultCampaignInfo($inputs);

        return array_merge(self::getDefaultCampaignInfo(), self::checkParams($params));
    }

    /**
     * Check and Validate Params
     * @param $params
     * @return array
     */
    public static function checkParams($params) : array
    {
        $output = [];

        if (isset($params['campaign_source']) && strlen($params['campaign_source']) > 0) {
            $output['campaign_source'] = $params['campaign_source'];
        }

        if (isset($params['campaign_medium']) && strlen($params['campaign_medium']) > 0) {
            $output['campaign_medium'] = $params['campaign_medium'];
        }

        if (isset($params['campaign_name']) && strlen($params['campaign_name']) > 0) {
            $output['campaign_name'] = $params['campaign_name'];
        }

        if (isset($params['campaign_term']) && strlen($params['campaign_term']) > 0) {
            $output['campaign_term'] = $params['campaign_term'];
        }

        if (isset($params['campaign_content']) && strlen($params['campaign_content']) > 0) {
            $output['campaign_content'] = $params['campaign_content'];
        }

        if (isset($params['campaign_cp']) && strlen($params['campaign_cp']) > 0) {
            $output['campaign_cp'] = $params['campaign_cp'];
        }

        return $output;
    }

    /**
     * Get Default Campaign Info
     * @return array
     */
    private static function getDefaultCampaignInfo() : array
    {
        $output = [
            'campaign_source' => self::getCampaignSource(),
            'campaign_medium' => self::getCampaignMedium(),
            'campaign_name' => self::getCampaignName(),
            'campaign_term' => self::getCampaignTerm(),
            'campaign_content' => self::getCampaignContent(),
            'campaign_cp' => self::getCampaignCp(),
        ];

        return $output;
    }

    /**
     * Set Default Campaign Info
     * @param array $params
     */
    private static function setDefaultCampaignInfo(array $params)
    {
        if (isset($params['utm_source']) && strlen($params['utm_source']) > 0) {
            self::setCampaignSource($params['utm_source']);
        }

        if (isset($params['utm_medium']) && strlen($params['utm_medium']) > 0) {
            self::setCampaignMedium($params['utm_medium']);
        }

        if (isset($params['utm_campaign']) && strlen($params['utm_campaign']) > 0) {
            self::setCampaignName($params['utm_campaign']);
        }

        if (isset($params['utm_term']) && strlen($params['utm_term']) > 0) {
            self::setCampaignTerm($params['utm_term']);
        }

        if (isset($params['utm_content']) && strlen($params['utm_content']) > 0) {
            self::setCampaignContent($params['utm_content']);
        }

        if (isset($params['cp']) && strlen($params['cp']) > 0) {
            self::setCampaignCp($params['cp']);
        }
    }
}
