<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use Illuminate\Support\Facades\Config;

/**
 * Class UtagDataDestination
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataDestination
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataDestination;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params = [])
    {
        self::setDefaultDestinationInfo();

        $output = self::getDefaultDestinationInfo();

        $output = array_merge($output, self::checkParams($params));

        return $output;
    }

    /**
     * Check and Validate Params
     * @param $params
     * @return array
     */
    public static function checkParams($params) : array
    {
        $output = [];

        if (isset($params['destination_continent']) && strlen($params['destination_continent']) > 0) {
            $output['destination_continent'] = $params['destination_continent'];
        }

        if (isset($params['destination_country']) && strlen($params['destination_country']) > 0) {
            $output['destination_country'] = $params['destination_country'];
        }

        if (isset($params['destination_country_code']) && strlen($params['destination_country_code']) > 0) {
            $output['destination_country_code'] = $params['destination_country_code'];
        }

        if (isset($params['destination_city']) && strlen($params['destination_city']) > 0) {
            $output['destination_city'] = $params['destination_city'];
        }

        if (isset($params['destination_area']) && strlen($params['destination_area']) > 0) {
            $output['destination_area'] = $params['destination_area'];
        }

        if (isset($params['destination_zone']) && strlen($params['destination_zone']) > 0) {
            $output['destination_zone'] = $params['destination_zone'];
        }

        return $output;
    }

    /**
     * Set Default Destination Info
     */
    private static function setDefaultDestinationInfo() : void
    {
        self::setDestinationContinent(Config::get('iberostar-tealium-iq.destination.destination_continent', ''));
        self::setDestinationCountry(Config::get('iberostar-tealium-iq.destination.destination_country', ''));
        self::setDestinationCountryCode(Config::get('iberostar-tealium-iq.destination.destination_country_code', ''));
        self::setDestinationCity(Config::get('iberostar-tealium-iq.destination.destination_city', ''));
        self::setDestinationArea(Config::get('iberostar-tealium-iq.destination.destination_area', ''));
        self::setDestinationZone(Config::get('iberostar-tealium-iq.destination.destination_zone', ''));
    }

    /**
     * Get Default Destination Info
     * @return array
     */
    private static function getDefaultDestinationInfo() : array
    {
        $output = [];

        if (strlen(self::getDestinationContinent()) > 0) {
            $output['destination_continent'] = self::getDestinationContinent();
        }

        if (strlen(self::getDestinationCountry()) > 0) {
            $output['destination_country'] = self::getDestinationCountry();
        }

        if (strlen(self::getDestinationCountryCode()) > 0) {
            $output['destination_country_code'] = self::getDestinationCountryCode();
        }

        if (strlen(self::getDestinationCity()) > 0) {
            $output['destination_city'] = self::getDestinationCity();
        }

        if (strlen(self::getDestinationArea()) > 0) {
            $output['destination_area'] = self::getDestinationArea();
        }

        if (strlen(self::getDestinationZone()) > 0) {
            $output['destination_zone'] = self::getDestinationZone();
        }

        return $output;
    }
}
