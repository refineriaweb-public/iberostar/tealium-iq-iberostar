<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use Illuminate\Http\Request;

/**
 * Class UtagDataPage
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataPage
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataPage;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params) : array
    {
        $output = [];
        $request = isset($params['request']) ? $params['request'] : new Request();
        $urlParse = parse_url(url()->full());
        if (!isset($urlParse['query'])) {
            $urlParse['query'] = '';
        }

        $fullURL = $request->url() . $_SERVER['REQUEST_URI'];

        self::setPageUrl($fullURL);
        self::setPageDomain($urlParse['host']);
        self::setPagePathname($request->path());
        self::setPageQueryString(isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '');
        self::setPageCanonical($request->url());

        $output['page_url'] = self::getPageUrl();
        $output['page_domain'] = self::getPageDomain();
        $output['page_pathname'] = self::getPagePathname();
        $output['page_query_string'] = self::getPageQueryString();
        $output['page_canonical'] = self::getPageCanonical();

        $output = array_merge($output, self::checkParams($params));

        return $output;
    }

    /**
     * Check and Validate Params
     * @param $params
     * @return array
     */
    public static function checkParams($params) : array
    {
        $output = [];

        if (isset($params['page_section'])) {
            $output['page_section'] = $params['page_section'];
        }

        if (isset($params['page_type'])) {
            $output['page_type'] = $params['page_type'];
        }

        if (isset($params['page_category'])) {
            $output['page_category'] = $params['page_category'];
        }

        if (isset($params['page_variation'])) {
            $output['page_variation'] = $params['page_variation'];
        }

        if (isset($params['page_title'])) {
            $output['page_title'] = htmlspecialchars($params['page_title'], ENT_QUOTES);
        }

        if (isset($params['page_description'])) {
            $output['page_description'] = htmlspecialchars($params['page_description'], ENT_QUOTES);
        }

        if (isset($params['page_language'])) {
            $output['page_language'] = $params['page_language'];
        }

        $output['page_currency'] = isset($params['page_currency']) ? isset($params['page_currency']) : 'EUR';
        $output['page_platform'] = isset($params['page_platform']) ? isset($params['page_platform']) : 'cms';
        $output['page_is_logged'] = isset($params['page_is_logged']) ? isset($params['page_is_logged']) : false;

        if (isset($params['page_site_origin'])) {
            $output['page_site_origin'] = $params['page_site_origin'];
        }

        return $output;
    }
}
