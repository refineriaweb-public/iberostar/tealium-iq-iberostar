<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use Illuminate\Support\Facades\Config;

/**
 * Class UtagData
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagData
{
    /** @var object  UtagData */
    public $utagData;

    /** @var string Environment */
    public $environment;

    /** @var array Product Impression List */
    public $productImpressionList;

    public function __construct(array $params)
    {
        $utagDataPage = UtagDataPage::getInfo($params);
        $utagDataDevice = UtagDataDevice::getInfo();
        $utagDataHotel = UtagDataHotel::getInfo($params);
        $utagDataKrux = UtagDataKrux::getInfo($params);
        $utagDataDestination = UtagDataDestination::getInfo($params);
        $utagDataCampaign = UtagDataCampaign::getInfo($params);
        $utagDataVisitor = UtagDataVisitor::getInfo($params);
        $utagDataProductImpressionList = UtagDataProductImpressionList::getInfo($params);
        $utagDataProduct = UtagDataProduct::getInfo($params);

        $this->utagData = (object) array_merge(
            $utagDataPage,
            $utagDataDevice,
            $utagDataHotel,
            $utagDataKrux,
            $utagDataDestination,
            $utagDataCampaign,
            $utagDataVisitor,
            $utagDataProduct
        );

        $this->environment = Config::get('iberostar-tealium-iq.environment', 'dev');
        $this->productImpressionList = $utagDataProductImpressionList;
    }
}
