<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

/**
 * Class UtagDataVisitor
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class UtagDataVisitor
{
    use \RefineriaWeb\TealiumIQIberostar\Traits\UtagDataVisitor;

    /**
     * Get Info
     * @param array $params Params
     * @return array
     */
    public static function getInfo(array $params = [])
    {
        $ip = $params['request']->ip();

        // INI - TESTING
//        $ip = "88.14.209.70"; // Spain
//        $ip = "68.14.209.70"; // United Sates
//        $params['request']->session()->forget('tealium_iq_visitor_ip'); // Testing
        // FIN - TESTING

        // Create Cookie
        $tealiumIQVisitorIP = session('tealium_iq_visitor_ip', null);
//        Log::info(json_encode($tealiumIQVisitorIP));

        if (!is_null($tealiumIQVisitorIP)) {
//            Log::info("Ya existe la session");
            self::setVisitorCountry($tealiumIQVisitorIP->country_name);
            self::setVisitorCountryCode($tealiumIQVisitorIP->country_code);

            if (!isset($tealiumIQVisitorIP->region_code)) {
                $tealiumIQVisitorIP->region_code = self::getRegionCode($tealiumIQVisitorIP->country_code);
                session(['tealium_iq_visitor_ip' => $tealiumIQVisitorIP]);
            }

            // @TODO - FIX temporal 'amer' por 'ame'
            if ('amer' === $tealiumIQVisitorIP->region_code) {
                $tealiumIQVisitorIP->region_code = 'ame';
            }

            self::setVisitorHeadquarterMarket($tealiumIQVisitorIP->region_code);

            return self::getDefaultVisitorInfo();
        }

//        Log::info("NO existe la session");

        // Utilizar servicio externo
        $json = @file_get_contents('https://geolocation-db.com/json/'.$ip);
        $ipInfo = json_decode($json);

        if ($ipInfo) {
            $ipInfo->region_code = self::getRegionCode($ipInfo->country_code);
            session(['tealium_iq_visitor_ip' => $ipInfo]);

            self::setVisitorCountry($ipInfo->country_name);
            self::setVisitorCountryCode($ipInfo->country_code);
            self::setVisitorHeadquarterMarket($ipInfo->region_code);
        }

        return self::getDefaultVisitorInfo();
    }


    /**
     * Get Default Visitor Info
     * @return array
     */
    private static function getDefaultVisitorInfo() : array
    {
        $output = [];

        if (strlen(self::getVisitorType()) > 0) {
            $output['visitor_type'] = self::getVisitorType();
        }

        if (strlen(self::getVisitorCountry()) > 0) {
            $output['visitor_country'] = self::getVisitorCountry();
        }

        if (strlen(self::getVisitorCountryCode()) > 0) {
            $output['visitor_country_code'] = self::getVisitorCountryCode();
        }

        $output['visitor_is_buyer'] = self::getVisitorIsBuyer();
        $output['visitor_has_reservation'] = self::getVisitorHasReservation();
        $output['visitor_is_logged'] = self::getVisitorIsLogged();
        $output['visitor_headquarter_market'] = self::getVisitorHeadquarterMarket();

        return $output;
    }

    /**
     * Get Region Code
     * @param string $countryCode
     * @return string
     */
    private static function getRegionCode(string $countryCode) : string
    {
        $countries = self::getCountries();

        if (isset($countries[$countryCode]["region_code"])) {
            return strtolower($countries[$countryCode]["region_code"]);
        }

        return "false";
    }

    /**
     * Get Countries
     * @return array
     */
    private static function getCountries() : array
    {
        return [
            "AD" => ["name" => "Andorra", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "AE" => ["name" => "United Arab Emirates", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "AF" => ["name" => "Afghanistan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "AG" => ["name" => "Antigua and Barbuda", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "AI" => ["name" => "Anguilla", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "AL" => ["name" => "Albania", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "AM" => ["name" => "Armenia", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "AO" => ["name" => "Angola", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "AQ" => ["name" => "Antarctica", "region_code" => "", "region" => "", "sub_region" => "", "intermediate_region" => ""],
            "AR" => ["name" => "Argentina", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "AS" => ["name" => "American Samoa", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "AT" => ["name" => "Austria", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "AU" => ["name" => "Australia", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Australia and New Zealand", "intermediate_region" => ""],
            "AW" => ["name" => "Aruba", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "AX" => ["name" => "√Öland Islands", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "AZ" => ["name" => "Azerbaijan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "BA" => ["name" => "Bosnia and Herzegovina", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "BB" => ["name" => "Barbados", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "BD" => ["name" => "Bangladesh", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "BE" => ["name" => "Belgium", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "BF" => ["name" => "Burkina Faso", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "BG" => ["name" => "Bulgaria", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "BH" => ["name" => "Bahrain", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "BI" => ["name" => "Burundi", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "BJ" => ["name" => "Benin", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "BL" => ["name" => "Saint Barth√©lemy", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "BM" => ["name" => "Bermuda", "region_code" => "AME", "region" => "Americas", "sub_region" => "Northern America", "intermediate_region" => ""],
            "BN" => ["name" => "Brunei Darussalam", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "BO" => ["name" => "Bolivia (Plurinational State of)", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "BQ" => ["name" => "Bonaire, Sint Eustatius and Saba", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "BR" => ["name" => "Brazil", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "BS" => ["name" => "Bahamas", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "BT" => ["name" => "Bhutan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "BV" => ["name" => "Bouvet Island", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "BW" => ["name" => "Botswana", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Southern Africa"],
            "BY" => ["name" => "Belarus", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "BZ" => ["name" => "Belize", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "CA" => ["name" => "Canada", "region_code" => "AME", "region" => "Americas", "sub_region" => "Northern America", "intermediate_region" => ""],
            "CC" => ["name" => "Cocos (Keeling) Islands", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Australia and New Zealand", "intermediate_region" => ""],
            "CD" => ["name" => "Congo (Democratic Republic of the)", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "CF" => ["name" => "Central African Republic", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "CG" => ["name" => "Congo", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "CH" => ["name" => "Switzerland", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "CI" => ["name" => "C√¥te d'Ivoire", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "CK" => ["name" => "Cook Islands", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "CL" => ["name" => "Chile", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "CM" => ["name" => "Cameroon", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "CN" => ["name" => "China", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "CO" => ["name" => "Colombia", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "CR" => ["name" => "Costa Rica", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "CU" => ["name" => "Cuba", "region_code" => "EMEA", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "CV" => ["name" => "Cabo Verde", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "CW" => ["name" => "Cura√ßao", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "CX" => ["name" => "Christmas Island", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Australia and New Zealand", "intermediate_region" => ""],
            "CY" => ["name" => "Cyprus", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "CZ" => ["name" => "Czechia", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "DE" => ["name" => "Germany", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "DJ" => ["name" => "Djibouti", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "DK" => ["name" => "Denmark", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "DM" => ["name" => "Dominica", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "DO" => ["name" => "Dominican Republic", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "DZ" => ["name" => "Algeria", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Northern Africa", "intermediate_region" => ""],
            "EC" => ["name" => "Ecuador", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "EE" => ["name" => "Estonia", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "EG" => ["name" => "Egypt", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Northern Africa", "intermediate_region" => ""],
            "EH" => ["name" => "Western Sahara", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Northern Africa", "intermediate_region" => ""],
            "ER" => ["name" => "Eritrea", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "ES" => ["name" => "Spain", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "ET" => ["name" => "Ethiopia", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "FI" => ["name" => "Finland", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "FJ" => ["name" => "Fiji", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Melanesia", "intermediate_region" => ""],
            "FK" => ["name" => "Falkland Islands (Malvinas)", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "FM" => ["name" => "Micronesia (Federated States of)", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "FO" => ["name" => "Faroe Islands", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "FR" => ["name" => "France", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "GA" => ["name" => "Gabon", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "GB" => ["name" => "United Kingdom of Great Britain and Northern Ireland", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "GD" => ["name" => "Grenada", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "GE" => ["name" => "Georgia", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "GF" => ["name" => "French Guiana", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "GG" => ["name" => "Guernsey", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => "Channel Islands"],
            "GH" => ["name" => "Ghana", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "GI" => ["name" => "Gibraltar", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "GL" => ["name" => "Greenland", "region_code" => "AME", "region" => "Americas", "sub_region" => "Northern America", "intermediate_region" => ""],
            "GM" => ["name" => "Gambia", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "GN" => ["name" => "Guinea", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "GP" => ["name" => "Guadeloupe", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "GQ" => ["name" => "Equatorial Guinea", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "GR" => ["name" => "Greece", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "GS" => ["name" => "South Georgia and the South Sandwich Islands", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "GT" => ["name" => "Guatemala", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "GU" => ["name" => "Guam", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "GW" => ["name" => "Guinea-Bissau", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "GY" => ["name" => "Guyana", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "HK" => ["name" => "Hong Kong", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "HM" => ["name" => "Heard Island and McDonald Islands", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Australia and New Zealand", "intermediate_region" => ""],
            "HN" => ["name" => "Honduras", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "HR" => ["name" => "Croatia", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "HT" => ["name" => "Haiti", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "HU" => ["name" => "Hungary", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "ID" => ["name" => "Indonesia", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "IE" => ["name" => "Ireland", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "IL" => ["name" => "Israel", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "IM" => ["name" => "Isle of Man", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "IN" => ["name" => "India", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "IO" => ["name" => "British Indian Ocean Territory", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "IQ" => ["name" => "Iraq", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "IR" => ["name" => "Iran (Islamic Republic of)", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "IS" => ["name" => "Iceland", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "IT" => ["name" => "Italy", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "JE" => ["name" => "Jersey", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => "Channel Islands"],
            "JM" => ["name" => "Jamaica", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "JO" => ["name" => "Jordan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "JP" => ["name" => "Japan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "KE" => ["name" => "Kenya", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "KG" => ["name" => "Kyrgyzstan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Central Asia", "intermediate_region" => ""],
            "KH" => ["name" => "Cambodia", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "KI" => ["name" => "Kiribati", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "KM" => ["name" => "Comoros", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "KN" => ["name" => "Saint Kitts and Nevis", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "KP" => ["name" => "Korea (Democratic People's Republic of)", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "KR" => ["name" => "Korea (Republic of)", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "KW" => ["name" => "Kuwait", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "KY" => ["name" => "Cayman Islands", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "KZ" => ["name" => "Kazakhstan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Central Asia", "intermediate_region" => ""],
            "LA" => ["name" => "Lao People's Democratic Republic", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "LB" => ["name" => "Lebanon", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "LC" => ["name" => "Saint Lucia", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "LI" => ["name" => "Liechtenstein", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "LK" => ["name" => "Sri Lanka", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "LR" => ["name" => "Liberia", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "LS" => ["name" => "Lesotho", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Southern Africa"],
            "LT" => ["name" => "Lithuania", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "LU" => ["name" => "Luxembourg", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "LV" => ["name" => "Latvia", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "LY" => ["name" => "Libya", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Northern Africa", "intermediate_region" => ""],
            "MA" => ["name" => "Morocco", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Northern Africa", "intermediate_region" => ""],
            "MC" => ["name" => "Monaco", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "MD" => ["name" => "Moldova (Republic of)", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "ME" => ["name" => "Montenegro", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "MF" => ["name" => "Saint Martin (French part)", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "MG" => ["name" => "Madagascar", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "MH" => ["name" => "Marshall Islands", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "MK" => ["name" => "Macedonia (the former Yugoslav Republic of)", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "ML" => ["name" => "Mali", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "MM" => ["name" => "Myanmar", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "MN" => ["name" => "Mongolia", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "MO" => ["name" => "Macao", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "MP" => ["name" => "Northern Mariana Islands", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "MQ" => ["name" => "Martinique", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "MR" => ["name" => "Mauritania", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "MS" => ["name" => "Montserrat", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "MT" => ["name" => "Malta", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "MU" => ["name" => "Mauritius", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "MV" => ["name" => "Maldives", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "MW" => ["name" => "Malawi", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "MX" => ["name" => "Mexico", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "MY" => ["name" => "Malaysia", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "MZ" => ["name" => "Mozambique", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "NA" => ["name" => "Namibia", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Southern Africa"],
            "NC" => ["name" => "New Caledonia", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Melanesia", "intermediate_region" => ""],
            "NE" => ["name" => "Niger", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "NF" => ["name" => "Norfolk Island", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Australia and New Zealand", "intermediate_region" => ""],
            "NG" => ["name" => "Nigeria", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "NI" => ["name" => "Nicaragua", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "NL" => ["name" => "Netherlands", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Western Europe", "intermediate_region" => ""],
            "NO" => ["name" => "Norway", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "NP" => ["name" => "Nepal", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "NR" => ["name" => "Nauru", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "NU" => ["name" => "Niue", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "NZ" => ["name" => "New Zealand", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Australia and New Zealand", "intermediate_region" => ""],
            "OM" => ["name" => "Oman", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "PA" => ["name" => "Panama", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "PE" => ["name" => "Peru", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "PF" => ["name" => "French Polynesia", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "PG" => ["name" => "Papua New Guinea", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Melanesia", "intermediate_region" => ""],
            "PH" => ["name" => "Philippines", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "PK" => ["name" => "Pakistan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Southern Asia", "intermediate_region" => ""],
            "PL" => ["name" => "Poland", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "PM" => ["name" => "Saint Pierre and Miquelon", "region_code" => "AME", "region" => "Americas", "sub_region" => "Northern America", "intermediate_region" => ""],
            "PN" => ["name" => "Pitcairn", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "PR" => ["name" => "Puerto Rico", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "PS" => ["name" => "Palestine, State of", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "PT" => ["name" => "Portugal", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "PW" => ["name" => "Palau", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "PY" => ["name" => "Paraguay", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "QA" => ["name" => "Qatar", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "RE" => ["name" => "R√©union", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "RO" => ["name" => "Romania", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "RS" => ["name" => "Serbia", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "RU" => ["name" => "Russian Federation", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "RW" => ["name" => "Rwanda", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "SA" => ["name" => "Saudi Arabia", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "SB" => ["name" => "Solomon Islands", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Melanesia", "intermediate_region" => ""],
            "SC" => ["name" => "Seychelles", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "SD" => ["name" => "Sudan", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Northern Africa", "intermediate_region" => ""],
            "SE" => ["name" => "Sweden", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "SG" => ["name" => "Singapore", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "SH" => ["name" => "Saint Helena, Ascension and Tristan da Cunha", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "SI" => ["name" => "Slovenia", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "SJ" => ["name" => "Svalbard and Jan Mayen", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Northern Europe", "intermediate_region" => ""],
            "SK" => ["name" => "Slovakia", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "SL" => ["name" => "Sierra Leone", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "SM" => ["name" => "San Marino", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "SN" => ["name" => "Senegal", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "SO" => ["name" => "Somalia", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "SR" => ["name" => "Suriname", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "SS" => ["name" => "South Sudan", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "ST" => ["name" => "Sao Tome and Principe", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "SV" => ["name" => "El Salvador", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Central America"],
            "SX" => ["name" => "Sint Maarten (Dutch part)", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "SY" => ["name" => "Syrian Arab Republic", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "SZ" => ["name" => "Eswatini", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Southern Africa"],
            "TC" => ["name" => "Turks and Caicos Islands", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "TD" => ["name" => "Chad", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Middle Africa"],
            "TF" => ["name" => "French Southern Territories", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "TG" => ["name" => "Togo", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Western Africa"],
            "TH" => ["name" => "Thailand", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "TJ" => ["name" => "Tajikistan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Central Asia", "intermediate_region" => ""],
            "TK" => ["name" => "Tokelau", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "TL" => ["name" => "Timor-Leste", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "TM" => ["name" => "Turkmenistan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Central Asia", "intermediate_region" => ""],
            "TN" => ["name" => "Tunisia", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Northern Africa", "intermediate_region" => ""],
            "TO" => ["name" => "Tonga", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "TR" => ["name" => "Turkey", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "TT" => ["name" => "Trinidad and Tobago", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "TV" => ["name" => "Tuvalu", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "TW" => ["name" => "Taiwan, Province of China", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Eastern Asia", "intermediate_region" => ""],
            "TZ" => ["name" => "Tanzania, United Republic of", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "UA" => ["name" => "Ukraine", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Eastern Europe", "intermediate_region" => ""],
            "UG" => ["name" => "Uganda", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "UM" => ["name" => "United States Minor Outlying Islands", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Micronesia", "intermediate_region" => ""],
            "US" => ["name" => "United States of America", "region_code" => "AME", "region" => "Americas", "sub_region" => "Northern America", "intermediate_region" => ""],
            "UY" => ["name" => "Uruguay", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "UZ" => ["name" => "Uzbekistan", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Central Asia", "intermediate_region" => ""],
            "VA" => ["name" => "Holy See", "region_code" => "EMEA", "region" => "Europe", "sub_region" => "Southern Europe", "intermediate_region" => ""],
            "VC" => ["name" => "Saint Vincent and the Grenadines", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "VE" => ["name" => "Venezuela (Bolivarian Republic of)", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "South America"],
            "VG" => ["name" => "Virgin Islands (British)", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "VI" => ["name" => "Virgin Islands (U.S.)", "region_code" => "AME", "region" => "Americas", "sub_region" => "Latin America and the Caribbean", "intermediate_region" => "Caribbean"],
            "VN" => ["name" => "Viet Nam", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "South-eastern Asia", "intermediate_region" => ""],
            "VU" => ["name" => "Vanuatu", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Melanesia", "intermediate_region" => ""],
            "WF" => ["name" => "Wallis and Futuna", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "WS" => ["name" => "Samoa", "region_code" => "EMEA", "region" => "Oceania", "sub_region" => "Polynesia", "intermediate_region" => ""],
            "YE" => ["name" => "Yemen", "region_code" => "EMEA", "region" => "Asia", "sub_region" => "Western Asia", "intermediate_region" => ""],
            "YT" => ["name" => "Mayotte", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "ZA" => ["name" => "South Africa", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Southern Africa"],
            "ZM" => ["name" => "Zambia", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"],
            "ZW" => ["name" => "Zimbabwe", "region_code" => "EMEA", "region" => "Africa", "sub_region" => "Sub-Saharan Africa", "intermediate_region" => "Eastern Africa"]];
    }
}
