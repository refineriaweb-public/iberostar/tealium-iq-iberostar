<?php

namespace RefineriaWeb\TealiumIQIberostar\Classes;

/**
 * Class Helpers
 * @package RefineriaWeb\TealiumIQIberostar\Classes
 */
class Helpers
{
    /**
     * Tealium IQ Promotion Info
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_promotion_id'   => (string) ["<id de la promoción>"]
     *      'tealiumiq_promotion_name'   => (string) ["<nombre de la promoción>"]
     *      'tealiumiq_promotion_creative'   => (string) ["<creatividad de la promoción>"]
     *      'tealiumiq_promotion_position' => (string) ["<posición promoción>"]
     *      'tealiumiq_event_cat' => (string)
     *      'tealiumiq_event_lbl' => (string)
     *    ]
     * @return string
     */
    public static function tealiumIQPromotionInfo(array $params = [])
    {
        $paramsProcesed = [];

        if (isset($params["tealiumiq_promotion_id"])) {
            $paramsProcesed["data-tealiumiq_promotion_id"] = $params["tealiumiq_promotion_id"];
        }

        if (isset($params["tealiumiq_promotion_name"])) {
            $paramsProcesed["data-tealiumiq_promotion_name"] = $params["tealiumiq_promotion_name"];
        }

        if (isset($params["tealiumiq_promotion_creative"])) {
            $paramsProcesed["data-tealiumiq_promotion_creative"] = $params["tealiumiq_promotion_creative"];
        }

        if (isset($params["tealiumiq_promotion_position"])) {
            $paramsProcesed["data-tealiumiq_promotion_position"] = $params["tealiumiq_promotion_position"];
        }

        if (isset($params["tealiumiq_event_cat"])) {
            $paramsProcesed["data-tealiumiq_event_cat"] = $params["tealiumiq_event_cat"];
        }

        if (isset($params["tealiumiq_event_lbl"])) {
            $paramsProcesed["data-tealiumiq_event_lbl"] = $params["tealiumiq_event_lbl"];
        }

        $output = "";
        array_walk($paramsProcesed, function ($item, $key) use (&$output) {
            $output .= $key . " = '" . $item . "' ";
        });

        return $output;
    }

    /**
     * Tealium IQ Event Call Center Click
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_event_cat'   => (string) "call center"
     *      'tealiumiq_event_act'   => (string) "<action_type_call_center>". click contact button|call me back|click to call
     *      'tealiumiq_event_lbl'   => (string) "<location_button>". pop up, header, footer, sticky
     *    ]
     * @return string
     */
    public static function tealiumIQEventCallCenter(array $params = [])
    {
        $paramsProcesed['dataset'] = [
            "tealiumiq_event_cat" => "call center",
            "tealiumiq_event_act" => "click contact button",
            "tealiumiq_event_lbl" => "header"
        ];

        if (isset($params["tealiumiq_event_cat"])) {
            $paramsProcesed['dataset']["tealiumiq_event_cat"] = $params["tealiumiq_event_cat"];
        }

        if (isset($params["tealiumiq_event_act"])) {
            $paramsProcesed['dataset']["tealiumiq_event_act"] = $params["tealiumiq_event_act"];
        }

        if (isset($params["tealiumiq_event_lbl"])) {
            $paramsProcesed['dataset']["tealiumiq_event_lbl"] = $params["tealiumiq_event_lbl"];
        }

        return " onclick='tealiumIQCallCenterEventClick(" . json_encode((object) $paramsProcesed) . ")' ";
    }

    /**
     * Tealium IQ Event Newsletter Submit
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_event_cat'   => (string) "newsletter"
     *      'tealiumiq_event_act'   => (string) "user suscribed"
     *      'tealiumiq_event_lbl'   => (string) ""
     *    ]
     * @return string
     */
    public static function tealiumIQEventNewsletter(array $params = [])
    {
        $paramsProcesed['dataset'] = [
            "tealiumiq_event_cat" => "newsletter",
            "tealiumiq_event_act" => "user suscribed",
            "tealiumiq_event_lbl" => ""
        ];

        if (isset($params["tealiumiq_event_cat"])) {
            $paramsProcesed['dataset']["tealiumiq_event_cat"] = $params["tealiumiq_event_cat"];
        }

        if (isset($params["tealiumiq_event_act"])) {
            $paramsProcesed['dataset']["tealiumiq_event_act"] = $params["tealiumiq_event_act"];
        }

        if (isset($params["tealiumiq_event_lbl"])) {
            $paramsProcesed['dataset']["tealiumiq_event_lbl"] = $params["tealiumiq_event_lbl"];
        }

        return " onclick='tealiumIQNewsletterEventClick(" . json_encode((object) $paramsProcesed) . ")' ";
    }

    /**
     * Tealium IQ Event Change Lang Click
     * @param string $lang Lang code
     * @return string
     */
    public static function tealiumIQEventChangeLang(string $lang)
    {
        $paramsProcesed['dataset'] = [
            "tealiumiq_event_cat" => "change page info",
            "tealiumiq_event_act" => "language",
            "tealiumiq_event_lbl" => $lang
        ];

        return " onclick='tealiumIQChangeLanguageEvent(" . json_encode((object) $paramsProcesed) . ")' ";
    }

    /**
     * Tealium IQ Event Call Center Click
     * @param string $roomName Room Name
     * @return string
     */
    public static function tealiumIQEventBookNow(string $roomName)
    {
        $paramsProcesed['dataset'] = [
            "tealiumiq_event_cat" => "book now",
            "tealiumiq_event_act" => "book room",
            "tealiumiq_event_lbl" => strtolower($roomName)
        ];

        return " onclick='tealiumIQBookNowEventClick(" . json_encode((object) $paramsProcesed) . ")' ";
    }

    /**
     * Tealium IQ Event
     * @param string $category Category
     *  Examples:
     *  "adsblocker"|"affiliates"|"availability options"|"booking quote"|"call center"|"checkin online"|
     *  "change page info"|"check availability"|"compare hotels"|"compare rooms"|"destinations"|"downloads"|
     *  "ecommerce"|"filters destinations"|"form"|"hotel information"|"jobs Iberostar"|"last search"|"lead"|
     *  "login"|"manage my booking"|"my iberostar gift"|"my room online"|"newsletter"|"not availability"|
     *  "offers"|"request reservation"|"rfp"|"searcher"|"search"|"social events"|
     * @param string $action Action
     * @param string|null $label Label
     * @return string
     */
    public static function tealiumIQEvent(string $category, string $action, string $label = null)
    {
        $paramsProcesed['dataset'] = [
            "tealiumiq_event_cat" => $category,
            "tealiumiq_event_act" => $action,
            "tealiumiq_event_lbl" => "",
        ];

        if (!is_null($label)) {
            $paramsProcesed['dataset'] ["tealiumiq_event_lbl"] = $label;
        }

        return " onclick='tealiumIQEvent(" . json_encode($paramsProcesed) . ")' ";
    }

    /**
     * Tealium IQ Event Multimedia Click
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_event_cat' => (string)
     *      'tealiumiq_event_act' => (string)
     *      'tealiumiq_event_lbl' => (string)
     *    ]
     * @return string
     */
    public static function tealiumIQEventMultimediaClick(array $params = [])
    {
        $paramsProcesed = [
            "data-tealiumiq_event_cat" => "hotel information",
            "data-tealiumiq_event_act" => "see multimedia info",
            "data-tealiumiq_event_lbl" => "photos"
        ];

        if (isset($params["tealiumiq_event_cat"])) {
            $paramsProcesed["data-tealiumiq_event_cat"] = $params["tealiumiq_event_cat"];
        }

        if (isset($params["tealiumiq_event_act"])) {
            $paramsProcesed["data-tealiumiq_event_act"] = $params["tealiumiq_event_act"];
        }

        if (isset($params["tealiumiq_event_lbl"])) {
            $paramsProcesed["data-tealiumiq_event_lbl"] = $params["tealiumiq_event_lbl"];
        }

        return " onclick='tealiumIQMultimediaClick(\"" .
            $paramsProcesed["data-tealiumiq_event_cat"] . "\", \"" .
            $paramsProcesed["data-tealiumiq_event_act"] . "\", \"".
            $paramsProcesed["data-tealiumiq_event_lbl"] . "\")' ";
    }

    /**
     * Tealium IQ Event Product Click
     * @param array $params Array containing the necessary params.
     *    $params = [
     *      'tealiumiq_enh_action' => (string) "product_click"
     *      'tealiumiq_event_cat' => (string) "ecommerce"
     *      'tealiumiq_event_act' => (string) "product_click"
     *      'tealiumiq_event_lbl' => (string) "<product name>"
     *      'tealiumiq_product_id' => (string) "<product id>"
     *      'tealiumiq_product_name' => (string) "<product name>"
     *      'tealiumiq_product_category' => (string) "<product category>"
     *      'tealiumiq_product_variant' => (string) "<variant>"
     *      'tealiumiq_product_brand' => (string) "<product brand>"
     *      'tealiumiq_product_action_list' => (string) "<lista en la que se encontraba el producto>"
     *      'tealiumiq_product_position' => (array) "[<position>]"
     *      'tealiumiq_product_unit_price' => string) "<product_unit_price>"
     *    ]
     * @return string
     */
    public static function tealiumIQEventProductClick(array $params = [])
    {
        $paramsProcesed['dataset'] = [
            "tealiumiq_enh_action" => "product_click",
            "tealiumiq_event_cat" => "ecommerce",
            "tealiumiq_event_act" => "product click",
            "tealiumiq_event_lbl" => "",
            "tealiumiq_product_id" => "",
            "tealiumiq_product_name" => "",
            "tealiumiq_product_category" => "",
            "tealiumiq_product_variant" => "",
            "tealiumiq_product_brand" => config('iberostar-tealium-iq.hotel.hotel_brand', ""),
            "tealiumiq_product_action_list" => "",
            "tealiumiq_product_position" => [0],
            "tealiumiq_product_unit_price" => 0,
        ];

        if (isset($params["tealiumiq_enh_action"])) {
            $paramsProcesed['dataset']["tealiumiq_enh_action"] = $params["tealiumiq_enh_action"];
        }

        if (isset($params["tealiumiq_event_cat"])) {
            $paramsProcesed['dataset']["tealiumiq_event_cat"] = $params["tealiumiq_event_cat"];
        }

        if (isset($params["tealiumiq_event_act"])) {
            $paramsProcesed['dataset']["tealiumiq_event_act"] = $params["tealiumiq_event_act"];
        }

        if (isset($params["tealiumiq_event_lbl"])) {
            $paramsProcesed['dataset']["tealiumiq_event_lbl"] = strtolower($params["tealiumiq_event_lbl"]);
            $paramsProcesed['dataset']["tealiumiq_product_name"] = $paramsProcesed['dataset']["tealiumiq_event_lbl"];
        }

        if (isset($params["tealiumiq_product_id"])) {
            $paramsProcesed['dataset']["tealiumiq_product_id"] = $params["tealiumiq_product_id"];
        }

        if (isset($params["tealiumiq_product_name"])) {
            $paramsProcesed['dataset']["tealiumiq_product_name"] = $params["tealiumiq_product_name"];
        }

        if (isset($params["tealiumiq_product_category"])) {
            $paramsProcesed['dataset']["tealiumiq_product_category"] = strtolower($params["tealiumiq_product_category"]);
        }

        if (isset($params["tealiumiq_product_variant"])) {
            $paramsProcesed['dataset']["tealiumiq_product_variant"] = $params["tealiumiq_product_variant"];
        }

        if (isset($params["tealiumiq_product_brand"])) {
            $paramsProcesed['dataset']["tealiumiq_product_brand"] = strtolower($params["tealiumiq_product_brand"]);
        }

        if (isset($params["tealiumiq_product_action_list"])) {
            $paramsProcesed['dataset']["tealiumiq_product_action_list"] = $params["tealiumiq_product_action_list"];
        }

        if (isset($params["tealiumiq_product_position"])) {
            $paramsProcesed['dataset']["tealiumiq_product_position"] = $params["tealiumiq_product_position"];
        }

        if (isset($params["tealiumiq_product_unit_price"])) {
            $paramsProcesed['dataset']["tealiumiq_product_unit_price"] = $params["tealiumiq_product_unit_price"];
        }

        return " onclick='tealiumIQProductEventClick(" . json_encode((object) $paramsProcesed) . ")' ";
    }
}
