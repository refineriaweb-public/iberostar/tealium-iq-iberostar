<?php

namespace RefineriaWeb\TealiumIQIberostar\Facades;

use Illuminate\Support\Facades\Facade;

class TealiumIQIberostarFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'UtagData';
    }
}
