<?php

namespace RefineriaWeb\TealiumIQIberostar;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class TealiumIQIberostarServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Include the package classmap autoloader
        if (File::exists(__DIR__ . '/../vendor/autoload.php')) {
            include __DIR__ . '/../vendor/autoload.php';
        }

        /**
         * Routes
         */
        $this->app->router->group([
            'namespace' => 'RefineriaWeb\TealiumIQIberostar\App\Http\Controllers'
        ], function () {
            require __DIR__ . '/routes/routes.php';
        });

        /**
         * Views
         * use: view('PackageName::view_name');
         */
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'TealiumIQIberostar');

        /*
        * php artisan vendor:publish
        * Existing files will not be published
        */
        // Publish views to resources/views/vendor/vendor-name/package-name
        $viewPathOutput = base_path('resources/views/vendor/refineriaweb/tealium-iq-iberostar');
        $this->publishes(
            [
                __DIR__ . '/resources/views' => $viewPathOutput,
            ]
        );

        // Publish assets to public/vendor/vendor-name/package-name
        $this->publishes([
            __DIR__.'/public' => public_path('vendor/refineriaweb/tealium-iq-iberostar'),
        ], 'public');

        // Publish configurations to config/vendor/vendor-name/package-name
        $this->publishes([
            __DIR__.'/config' => config_path('/'),
        ], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tealium-iq-iberostar', 'RefineriaWeb\TealiumIQIberostar\TealiumIQIberostar');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */

    public function provides()
    {
        return ['tealium-iq-iberostar'];
    }
}
