<?php

return [

    /*
     * Indicar el entorno. Dos posibilidades: "dev" o "prod"
     * Es importante remarcar que el snnipet de dev se pondrá en aquellos entornos en los que se necesite validar la analítica
     * y el de prod solamente en un entorno de Producción real, independientemente de la plataforma (CRS o CMS).
     */
    'environment' => env('IB_TEALIUM_IQ_ENVIRONMENT', 'dev'),

    /*
    |--------------------------------------------------------------------------
    | Hotel
    |--------------------------------------------------------------------------
    |
    | Hotel Info
    |
    */
    'hotel' => [

        /*
         * Gestora inicial, se mantiene constante. Si aterriza en ficha de hotel, setear con gestora del hotel,
         * si aterriza en página neutra (como home) setear con localización del usuario.
         * Ejemplo: AME
         */
        'hotel_headquarter_origin' => env('IB_TEALIUM_IQ_HOTEL_HEADQUARTER_ORIGIN', ''),

        /*
         * Gestora final. Inicialmente coge el valor de la inicial, pero si se cambia durante la navegación se tiene que actualizar.
         * Solo cambiará si la gestora de un hotel que está viendo pertenece a una gestora diferente a la que ya tenía.
         * Ejemplo: EMEA
         */
        'hotel_headquarter_final' => env('IB_TEALIUM_IQ_HOTEL_HEADQUARTER_FINAL', ''),

        /*
         * Código SKU del hotel
         * Ejemplo: 1234
         */
        'hotel_sku' => env('IB_TEALIUM_IQ_HOTEL_SKU', ''),

        //
        /*
         * Nombre del hotel completo
         * Ejemplo: iberostar bouganville playa
         */
        'hotel_name' => env('IB_TEALIUM_IQ_HOTEL_NAME', ''),

        /*
         * Nombre corto del hotel
         * Ejemplo: HDL
         */
        'hotel_short_name' => env('IB_TEALIUM_IQ_HOTEL_SHORT_NAME', ''),

        /*
         * Tipo de hotel.
         * Ejemplo: hotel, apartment, aparthotel, hotel & bungalows
         */
        'hotel_type' => env('IB_TEALIUM_IQ_HOTEL_TYPE', ''),

        // Hotel sólo para adultos o no
        /*
         * Hotel sólo para adultos o no
         * Ejemplo: true or false
         */
        'hotel_is_only_adults' => env('IB_TEALIUM_IQ_HOTEL_IS_ONLY_ADULTS', false),


        /*
         * Número de estrellas (categoría) del hotel
         * Ejemplo: 5
         */
        'hotel_stars' => env('IB_TEALIUM_IQ_HOTEL_STARS', 5),

        /*
         * Categoria de hotel. Posibles valores: beaches hotels, hotels with history, city hotel
         * Ejemplo: beaches hotels
         */
        'hotel_segment' => env('IB_TEALIUM_IQ_HOTEL_SEGMENT', ''),

        /*
         * Subcategoria de hotel. Si es en la playa, ciudad, etc.
         * Posibles valores: iberostar, iberostar grand, iberostar grand heritage, iberostar heritage y iberostar selection
         * Ejemplo: iberostar grand
         */
        'hotel_subsegment' => env('IB_TEALIUM_IQ_HOTEL_SUBSEGMENT', ''),

        /*
         * Marca del hotevent_label: iberostar, ole, tropicana, cartago, etc
         * Ejemplo: las letras
         */
        'hotel_brand' => env('IB_TEALIUM_IQ_HOTEL_BRAND', ''),

        /*
         * Array de servicios que incorpora el hotel.
         * Ejemplo: wifi premium, splash pool Indoor Pool, Sea front hotel
         */
        'hotel_services' => env('IB_TEALIUM_IQ_HOTEL_SERVICES', ''),
    ],

    /*
    |--------------------------------------------------------------------------
    | Krux
    |--------------------------------------------------------------------------
    |
    | Krux Info
    |
    */
    'krux' => [

        /*
         * Identificador de Krux. Se recoge del localstorage
         */
        'hotel_brand' => env('IB_TEALIUM_IQ_KRUX_ID', ''),

        /*
         * Segmentos de Krux. Se recoge del localstorage
         */
        'krux_segments' => env('IB_TEALIUM_IQ_KRUX_SEGMENTS', ''),
    ],

    /*
    |--------------------------------------------------------------------------
    | Destination
    |--------------------------------------------------------------------------
    |
    | Destination Info
    |
    */
    'destination' => [

        /*
         * Continente al cual pertenece el destino que se esta visualizando o se ha reservado.
         * Ejemplo: europe
         */
        'destination_continent' => env('IB_TEALIUM_IQ_DESTINATION_CONTINENT', ''),

        /*
         * País al cual pertenece el destino que se esta visualizando o se ha reservado.
         * En el caso de bodas será el país del menú en el cual estás navegando.
         * Ejemplo: spain
         */
        'destination_country' => env('IB_TEALIUM_IQ_DESTINATION_COUNTRY', ''),

        /*
         * Código ISO (2 letras) del país que se esta visualizando o se ha reservado.
         * En mayusculas.
         * Ejemplo: ES
         */
        'destination_country_code' => env('IB_TEALIUM_IQ_DESTINATION_COUNTRY_CODE', ''),

        /*
         * Ciudad a la que pertenece. Esta variable seguirá la estructura jerárquica de la web es decir.
         * Para barcelona, europe será el destination_continent, spain el destination_country, barcelona el destination_city, no tendrá destination_area y destination_zone.
         *  En el caso de playa de muro, sera: destination_continent europe, destination_country spain, destination_city balearic islands, destination_area mallorca y destination_zone playa de muro.
         */
        'destination_city' => env('IB_TEALIUM_IQ_DESTINATION_CITY', ''),

        /*
         * Área a la cual pertenece el destino que se esta visualizando o se ha reservado.
         * Ejemplo: majorca
         */
        'destination_area' => env('IB_TEALIUM_IQ_DESTINATION_AREA', ''),

        /*
         * Zona a la cual pertenece el destino que se esta visualizando o se ha reservado.
         * Ejemplo: playa de muro
         */
        'destination_zone' => env('IB_TEALIUM_IQ_DESTINATION_ZONE', ''),
    ],
];
