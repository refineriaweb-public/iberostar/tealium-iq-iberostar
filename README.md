# Laravel Tealium IQ Iberostar
=======================

## Installation
For use with Laravel 5. This package can be installed through Composer.

```json
{
    "require": {
		"refineriaweb/tealium-iq-iberostar": "master"
	},

	"repositories": [
        {
          "type": "git",
          "url": "git@gitlab.com:refineriaweb/iberostar/tealium-iq-iberostar.git"
        }
    ]
}
```

You must install this service provider <code>config/app.php</code>.

```php
'providers' => [
    '...',
    RefineriaWeb\TealiumIQIberostar\TealiumIQIberostarServiceProvider::class,
];
```

This package also comes with a facade, which provides an easy way to call the the class <code>config/app.php</code>.


```php
'aliases' => [
	'...',
	'TealiumIQIberostar' => RefineriaWeb\TealiumIQIberostar\TealiumIQIberostarFacade::class,
]
```

Publish the config file of the package using artisan

```bash
php artisan vendor:publish --provider="RefineriaWeb\TealiumIQIberostar\TealiumIQIberostarServiceProvider" --tag=config
```

After the config file has been created locate at:

```
/config/tealium-iq-iberostar.php
```

Publish public files

```bash
php artisan vendor:publish --provider="RefineriaWeb\TealiumIQIberostar\TealiumIQIberostarServiceProvider" --tag=public
```

Add scripts in blade layout
```blade
@include('TealiumIQIberostar::scripts')
```
